﻿using AutoMapper;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.Pages.GeneralSettings;
using Plumbago_Brokerage.Pages.HumanResources;
using Plumbago_Brokerage.Pages.KPIManager;
using Plumbago_Brokerage.Pages.Payroll;
using Plumbago_Brokerage.Pages.SelfService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.Profiles
{
    public class Plumbago_BrokerageProfile : Profile
    {
        public Plumbago_BrokerageProfile()
        {
            CreateMap<Users, Users_editModel>().ReverseMap();
            CreateMap<Loans, Loan_request_editModel>().ReverseMap();
            CreateMap<Leave, Leave_request_editModel>().ReverseMap();
            CreateMap<Expenses, Reimbursement_editModel>().ReverseMap();
            CreateMap<Employees, Employees_editModel>().ForMember(x => x.Image, opt => opt.Ignore()).ReverseMap();
            CreateMap<Education, Qualifications_EditModel>().ReverseMap();
            CreateMap<Promotion, EmployeePayroll_EditModel>().ReverseMap();
            CreateMap<EmployeePayroll_EditModel, Employees>().ReverseMap();
            CreateMap<Interests_EditModel, Hobbies>().ReverseMap();
            CreateMap<EmployeeGradeLevel_EditModel, Promotion>().ReverseMap();
            CreateMap<EmployeeMedicalDetails_EditModel, MedDetails>().ReverseMap();
            CreateMap<EmployeeMedicalProcessing_EditModel, Medical>().ReverseMap();
            CreateMap<SelfAppraisal_EditModel, Appraisal>().ReverseMap();
            CreateMap<SelfAppraisal_EditModel, AppraisalDetails>().ReverseMap();
            CreateMap<TrainingNeeds_EditModel, Trainings>().ReverseMap();
            CreateMap<LeaveRequestsProcessing_EditModel, Leave>().ReverseMap();
            CreateMap<LoanRequestsProcessing_EditModel, Loans>().ReverseMap();
            CreateMap<KPIs_EditModel, Kpi>().ReverseMap();
            CreateMap<AllowanceRates_EditModel, PayAllowance>().ReverseMap();
            CreateMap<DeductionRates_EditModel, PayDeductions>().ReverseMap();
            CreateMap<EmployeePayrollDetails_EditModel, Employees>().ReverseMap();
            CreateMap<MonthlyVariance_EditModel, PaySlips>().ReverseMap();
            CreateMap<EmployeePayrollDetails_EditModel, PaySlips>().ReverseMap();

        }
       

    }
}
