using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Reports.HumanResources;
using Plumbago_Brokerage.Reports.Payroll;

namespace Plumbago_Brokerage.Pages.Report
{
    public class ViewerModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public XtraReport Report { get; set; }
        public IActionResult OnGet(string reportid)
        {
            if (reportid == "EmployeeLoanReport")
            {
                Report = new EmployeeLoanReport();              
            }
            if (reportid == "RptAllDisengagedEmployee")
            {
                Report = new RptAllDisengagedEmployees();
            }
            if (reportid == "RptAllEmployedStaffByPeriod")
            {
                Report = new RptAllEmployedStaffByPeriod();
            }
            if (reportid == "RptAllEmployeeBirthday")
            {
                Report = new RptAllEmployeeBirthday();
            }
            if (reportid == "RptAllEmployeeDiscipline")
            {
                Report = new RptAllEmployeeDiscipline();
            }
            if (reportid == "RptEmployeeAppraisal")
            {
                Report = new RptEmployeeAppraisal();
            }
            if (reportid == "RptEmployeeTrainingCostSummary")
            {
                Report = new RptEmployeeTrainingCostSummary();
            }

            if (reportid == "RptEmployeePaySlip")
            {
                Report = new RptEmployeePaySlip();
            }
            if (reportid == "RptEmployeeLeaveDetailsByBranch")
            {
                Report = new RptEmployeeLeaveDetailsByBranch();
            }
            if (reportid == "RptEmployeeMedicalDetails")
            {
                Report = new RptEmployeeMedicalDetails();
            }

            return Page();
        }
    }
}
