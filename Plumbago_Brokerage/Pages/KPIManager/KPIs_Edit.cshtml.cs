using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.KPIManager
{
    [BindProperties]
    public class KPIs_EditModel : PageModel
    {
        public int Id { get; set; }
        public string Department { get; set; }
        public string Period { get; set; }
        public string KpiContent { get; set; }
        public IEnumerable<SelectListItem> Periods { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        private readonly IMapper _Mapper;

        public KPIs_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();
            if (id != null)
            {
                var kpirepo = new KPIRepo().GetKPIByID(id);
                _Mapper.Map(kpirepo, this);
                Periods = mod.PopulatePeriod();
                Departments = mod.PopulateDepartments();
                EditModel.PageTitle = $"Edit KPI {id}";
                return Page();
            }
            EditModel.PageTitle = "Create New KPI";          
            Periods = mod.PopulatePeriod();
            Departments = mod.PopulateDepartments();
            return Page();
        }

        public IActionResult OnPostSave()
        {
            if (!ModelState.IsValid) return Page();
            if (DoInsert())
            {
                return RedirectToPage("KPIs");
            }
            else
            {
                return Page();
            }
        }

        private bool DoInsert()
        {
            ModMain mod = new ModMain();
            var kpirepo = new KPIRepo();
            if (Id > 0)
            {
                //update
                var kpi = kpirepo.GetKPIByID(Id);                
                _Mapper.Map(this, kpi);

                int result = kpirepo.UpdateKPI(kpi, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return true;
                }
                else
                {
                    EditModel.ErrMsg = mod.GetErrorMessage();
                    return false;
                }
            }
            else
            {
                Kpi kpi = new Kpi();              
                _Mapper.Map(this, kpi);
                int result = kpirepo.CreateKPI(kpi, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return true;
                }
                else
                {
                    EditModel.ErrMsg = mod.GetErrorMessage();
                    return false;
                }


            }
        }

    }
}
