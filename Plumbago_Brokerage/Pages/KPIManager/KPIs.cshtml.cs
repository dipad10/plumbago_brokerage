using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.KPIManager
{
    public class KPIsModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }

        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "KPM_kpi";
            TableList.Groupname = "KPM_kpi";
            Searchfilter.Departments = mod.PopulateDepartments();
            Searchfilter.Periods = mod.PopulatePeriod();
            TableList.Kpis = new KPIRepo().GetKPIBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("KPIs", new { Searchfilter.From, Searchfilter.To, Searchfilter.Period, Searchfilter.Department });
        }
    }
}
