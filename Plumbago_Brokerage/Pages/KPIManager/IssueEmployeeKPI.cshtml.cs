using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.KPIManager
{
    public class IssueEmployeeKPIModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }
        public string ErrMsg { get; set; }

        public string ErrMsgType { get; set; }

        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "KPM_Issue";
            TableList.Groupname = "KPM_Issue";
            Searchfilter.Branches = mod.PopulateBranches();
            Searchfilter.Departments = mod.PopulateDepartments();
            TableList.Employees = new EmployeesRepo().GetEmployeesBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("IssueEmployeeKPI", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option });
        }

        public IActionResult OnPostPrint()
        {
            ModMain mod = new ModMain();
            var kpirepo = new KPIRepo();
            if (SelectedIDs.Count == 0)
            {
                TempData["error"] = true;
                TempData["message"] = "Select an Employee first";
               
                Searchfilter.Groupname = "KPM_Issue";
                TableList.Groupname = "KPM_Issue";
                Searchfilter.Branches = mod.PopulateBranches();
                Searchfilter.Departments = mod.PopulateDepartments();
                TableList.Employees = new EmployeesRepo().GetEmployeesBySearchFilter(Searchfilter);
                return Page();
            }
            foreach (var id in SelectedIDs)
            {
                
            }
            TempData["success"] = true;
            TempData["message"] = mod.GetSuccessMessage();
            return RedirectToPage("IssueEmployeeKPI");
        }
    }
}
