using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    [BindProperties]
    public class LeaveRequestsProcessing_EditModel : PageModel
    {
        public int LeaveId { get; set; }
        public string EmployeeId { get; set; }
        public string Surname { get; set; }
        public string Othernames { get; set; }
        public string LeaveType { get; set; }
        public DateTime? Ldate { get; set; }
        public long? Duration { get; set; }
        public string Dmy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? LeaveArrears { get; set; }
        public decimal? LeaveAllow { get; set; }
        public string Purpose { get; set; }
        public string TransStatus { get; set; }
        public string Remarks { get; set; }
        public string Tag { get; set; }
        public IEnumerable<SelectListItem> LeaveTypes { get; set; }
        public IEnumerable<SelectListItem> Employees { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }

        private readonly IMapper _Mapper;

        public LeaveRequestsProcessing_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();
            if (id != null)
            {

                var leave = new LeaveRepo().GetLeaveByID(id);
                _Mapper.Map(leave, this);
                EditModel.PageTitle = $"Edit Leave: {id}";
                LeaveTypes = mod.PopulateLeaveTypes();
                Employees = mod.PopulatEmployees();
                return Page();
            }
            LeaveTypes = mod.PopulateLeaveTypes();
            Employees = mod.PopulatEmployees();
            EditModel.PageTitle = $"Create New Employee Leave";
            return Page();
        }
    }
}
