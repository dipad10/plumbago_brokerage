using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    [BindProperties]
    public class Qualifications_EditModel : PageModel
    {
        public int EducationId { get; set; }
        public string EmployeeNo { get; set; }
        public string SchoolAttend { get; set; }
        public DateTime? Dfrom { get; set; }
        public DateTime? Dto { get; set; }
        public string CertObtained { get; set; }
        public string Comment { get; set; }
        public IEnumerable<SelectListItem> Certs { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        private readonly IMapper _Mapper;

        public Qualifications_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }

        public override void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
                RedirectToPage("/Welcome/Login");
            }
        }
        public IActionResult OnGet(int? id, int? detailid)
        {
            var mod = new ModMain();
            var EducationRepo = new EducationRepo();
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
               return RedirectToPage("/Welcome/Login");
            }
            //for toptabs
            EditModel.Groupname = "hr_employees";
            EditModel.Editid = id.ToString();

            TableList.Groupname = "hr_Qualifications_edit";
            TableList.Educations = EducationRepo.GetAllEmployeeEducations(id.ToString());
            EmployeeNo = id.ToString();
            if (id != null)
            {
                //fetch employee top education
                //var education = EducationRepo.GetEducationByEmployeeID(id.ToString());
                //_Mapper.Map(education, this);
                if (detailid != null)
                {
                   var education = EducationRepo.GetEducation(detailid, id.ToString());
                    _Mapper.Map(education, this);
                    EditModel.PageTitle = $"Edit Qualification for Employee: {id} and Detail: { detailid}";
                }
                else
                {
                    EditModel.PageTitle = $"Edit Qualification for Employee: {id}";
                }
               
                Certs = new ModMain().PopulateCertifications();
                return Page();
            }
            EditModel.PageTitle = "Add new Education";
            Certs = new ModMain().PopulateCertifications();
            return Page();
        }
    }
}
