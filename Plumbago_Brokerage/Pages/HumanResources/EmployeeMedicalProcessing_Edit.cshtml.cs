using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeeMedicalProcessing_EditModel : PageModel
    {
        public int MedId { get; set; }
        public string EmployeeId { get; set; }
        public DateTime? MedDate { get; set; }
        public bool AdmType { get; set; }
        public long? Days { get; set; }
        public string TreatDesc { get; set; }
        public string MedHospital { get; set; }
        public decimal? MedicalLimit { get; set; }
        public decimal? MedicalCost { get; set; }
        public decimal? LimitBalance { get; set; }
        public DateTime? StartDate { get; set; } = DateTime.Now;
        public DateTime? EndDate { get; set; } = DateTime.Now;
        public string Remarks { get; set; }
        public IEnumerable<SelectListItem> Employees { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }

        private readonly IMapper _Mapper;

        public EmployeeMedicalProcessing_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();            
            if (id != null)
            {

                var medical = new MedicalRepo().GetMedicalByID(id);
                _Mapper.Map(medical, this);
                EditModel.PageTitle = $"Edit Medical Event for Employee: {EmployeeId}";
                Employees = mod.PopulatEmployees();
                return Page();
            }
            Employees = mod.PopulatEmployees();
            EditModel.PageTitle = $"Create New Medical Activity";
            return Page();
        }
    }
}
