using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeeMedicalDetails_EditModel : PageModel
    {
        public long MedDetailsId { get; set; }
        public string EmployeeNo { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Bloodgrp { get; set; }
        public bool? Disable { get; set; }
        public string Type { get; set; }
        public string Allergics { get; set; }
        public decimal? Medicallimits { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        private readonly IMapper _Mapper;

        public EmployeeMedicalDetails_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id, int? detailid)
        {
            //for toptabs
            var medRepo = new MedicalDetailsRepo();
            EditModel.Groupname = "hr_employees";
            EditModel.Editid = id.ToString();

            TableList.Groupname = "hr_medicals_edit";
            TableList.MedDetails = medRepo.GetAllEmployeeMedicalDetails(id.ToString());
            if (id != null)
            {
                //fetch employee top hobbies

                if (detailid != null)
                {
                    var meddetails = medRepo.GetMedDetailsByEmployeeIDAndHobbyID(id.ToString(), (long)detailid);
                    _Mapper.Map(meddetails, this);
                    EditModel.PageTitle = $"Edit Medical Details for Employee: {id} and Detail: {detailid}";
                }
                else
                {
                    EditModel.PageTitle = $"Edit Medical Details for Employee: {id}";
                }

                return Page();
            }

            EditModel.PageTitle = "Add new Employee Medical details";
            return Page();
        }
    }
}
