using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeePromotionsModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "hr_employee_promotions";
            TableList.Groupname = "hr_employee_promotions";
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            TableList.PromotionsQueryable = new PromotionRepo().GetEmployeesBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("EmployeePromotions", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet });
        }
    }
}
