using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeeDocuments_EditModel : PageModel
    {
        public int DocumentId { get; set; }
        public string EmployeeId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentPath { get; set; }
     
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        private readonly IMapper _Mapper;

        public EmployeeDocuments_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id, int? detailid)
        {
            var mod = new ModMain();
            var DocRepo = new DocumentsRepo();
            EditModel.Groupname = "hr_employees";
            EditModel.Editid = id.ToString();
            TableList.Groupname = "hr_documents_edit";
            TableList.Documents = DocRepo.GetAllEmployeeDocuments(id.ToString());
           
            EditModel.PageTitle = "Add new Employee Documents";
            return Page();
        }
    }
}
