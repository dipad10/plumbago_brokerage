using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeeGradeLevel_EditModel : PageModel
    {
        public long PromotionId { get; set; }
        public int? SheetId { get; set; }
        public string EmployeeId { get; set; }
        public string Grade { get; set; }
        public string Step { get; set; }
        public DateTime? ProDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Bs { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Ha { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Ta { get; set; }
        public decimal? Ygpay { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Ytax { get; set; }
        public string TransStatus { get; set; }
        public string TransGuid { get; set; }
        public string Remarks { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public IEnumerable<SelectListItem> Grades { get; set; }
        public IEnumerable<SelectListItem> Paysheets { get; set; }

        private readonly IMapper _Mapper;

        public EmployeeGradeLevel_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id, int? detailid)
        {
            //for toptabs
            var mod = new ModMain();
            var PromotionRepo = new PromotionRepo();
            EditModel.Groupname = "hr_employees";
            EditModel.Editid = id.ToString();
            TableList.Groupname = "hr_EmployeeGradeLevel_Edit";
            TableList.Promotions = PromotionRepo.GetEmployeePromotions(id.ToString());
            if (id != null)
            {
                //fetch employee top hobbies
               
                Grades = mod.PopulateGradeLevel();
                Paysheets = mod.PopulatePaySheets();
                EmployeeId = id.ToString();
                //If detail id is not empty then load the edit data
                if (detailid != null)
                {
                    var promotions = PromotionRepo.GetPromotionByEmployeeIDandEditID(id.ToString(), (long)detailid);
                    _Mapper.Map(promotions, this);
                    Grades = mod.PopulateGradeLevel();
                    Paysheets = mod.PopulatePaySheets();
                    EditModel.PageTitle = $"Edit Employee Promotions: {id} and Detail: { detailid}";
                }
                else
                {
                    EditModel.PageTitle = $"Edit Employee Promotions: {id}";
                }

                return Page();
            }
            Grades = mod.PopulateGradeLevel();
            Paysheets = mod.PopulatePaySheets();
            EditModel.PageTitle = "Add new Employee Promotion and Grade Level";
            return Page();
        }
    }
}
