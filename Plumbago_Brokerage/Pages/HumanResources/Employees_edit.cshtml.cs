﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Plumbago_Brokerage.Data;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    [BindProperties]
    public class Employees_editModel : PageModel
    {
        public string EmployeeId { get; set; }
        [Required]
        public string Username { get; set; }
        public string StaffIdno { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string OtherNames { get; set; }
        public string Address { get; set; }
        [Required]
        public string State { get; set; }
       
        public string Country { get; set; }
        [Required]
        public string Sex { get; set; }
        [Required]
        public DateTime? BirthDate { get; set; } = DateTime.Now;
        [Required]
        public string MaritalStatus { get; set; }
        [Required]
        public string StateOrigin { get; set; }
        public string NationalIdno { get; set; }

        public string AcctNo1 { get; set; }

        public string AcctName1 { get; set; }
        public string AcctNo2 { get; set; }
        public string AcctName2 { get; set; }
        public string BranchId { get; set; }
        public string Branch { get; set; }

        public string DeptId { get; set; }
        public string Paysheet { get; set; }
        public string Dept { get; set; }
    
        public string UnitId { get; set; }
        public string Unit { get; set; }
       
        public long? GradeId { get; set; }
        public string Grade { get; set; }
        public DateTime? HireDate { get; set; } = DateTime.Now;
        public string Telephone { get; set; }
        [Required]
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string NextKin { get; set; }
        public string KinAddress { get; set; }
        public string KinPhone { get; set; }
        public string KinRelationship { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public long? Smoker { get; set; }
        public string DisableType { get; set; }
        public string Remarks { get; set; }
        public string Step { get; set; }
        public string Tag { get; set; }
        public string PayFirstMonth { get; set; }
        public long? SheetId2 { get; set; }
        public string ConfirmStatus { get; set; }
        public long? ConfirmDuration { get; set; }
        public DateTime? ConfirmationDate { get; set; } = DateTime.Now;
        public DateTime? RetiredDate { get; set; }
        public byte? Deleted { get; set; }
        public byte? Active { get; set; }
        public string Hmoname { get; set; }
        public string Hmoid { get; set; }
        public string Email2 { get; set; }
        [Required]
        public string PrimaryManager { get; set; }
        public string PrimaryManagerId { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? AnnualTax { get; set; } = 0;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? BasicSalary { get; set; } = 0;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? TransportAllowance { get; set; } = 0;
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? HousingAllowance { get; set; } = 0;
        public IEnumerable<SelectListItem> Employees { get; set; }
        [DataType(DataType.Upload)]
        public IFormFile Image { get; set; }
        public string ImageUrl { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public IEnumerable<SelectListItem> HMOLists { get; set; }
        public IEnumerable<SelectListItem> PFALists { get; set; }
        public IEnumerable<SelectListItem> Paygroups { get; set; }
        public IEnumerable<SelectListItem> GradeLevel { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }
        public IEnumerable<SelectListItem> Steps { get; set; }
    
        public IEnumerable<SelectListItem> Units { get; set; }
        public IEnumerable<SelectListItem> Branches { get; set; }
        public IEnumerable<SelectListItem> banks { get; set; }
        public string[] ConfirmStatuses = new[] { "Active", "Resigned", "Suspended", "Retired" };
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }

        private readonly IMapper _Mapper;

        private readonly IWebHostEnvironment _environment;
        public Employees_editModel(IMapper Mapper, IWebHostEnvironment environment)
        {
            _Mapper = Mapper;
            _environment = environment;
        }


        public IActionResult OnGet(int? id)
        {
            //for toptabs
            EditModel.Groupname = "hr_employees";
            EditModel.Editid = id.ToString();
            var mod = new ModMain();
            if (id != null)
            {

                var employee = new EmployeesRepo().GetEmployeeByID(id.ToString());
                ImageUrl = employee.Image;
            
                _Mapper.Map(employee, this);
                EditModel.PageTitle = $"Edit Employee: {Surname} {OtherNames}";
                States = mod.PopulateStates();
                HMOLists = mod.PopulateHMO();
                PFALists = mod.PopulatePFA();
                Paygroups = mod.PopulatePaySheets();
                GradeLevel = mod.PopulateGradeLevel();
                Departments = mod.PopulateDepartments();
                Units = mod.PopulateUnits();
                Branches = mod.PopulateBranches();
                Employees = mod.PopulatEmployees();
                Steps = mod.PopulateSteps();
                banks = mod.PopulateBanks();
                return Page();
            }
            States = mod.PopulateStates();
            HMOLists = mod.PopulateHMO();
            PFALists = mod.PopulatePFA();
            Paygroups = mod.PopulatePaySheets();
            GradeLevel = mod.PopulateGradeLevel();
            Departments = mod.PopulateDepartments();
            Units = mod.PopulateUnits();
            Branches = mod.PopulateBranches();
            Employees = mod.PopulatEmployees();
            Steps = mod.PopulateSteps();
            banks = mod.PopulateBanks();
            EditModel.PageTitle = "Create new Employee";
            return Page();
        }

        public IActionResult OnPostSave()
        {
            //if (!ModelState.IsValid)
            //{
            //    return Page();
            //}
            if (DoInsert())
            {
                return RedirectToPage("EmployeeGradeLevel_Edit", new { id = EmployeeId });
            }
            else
            {
                return Page();
            }
        }

        private bool DoInsert()
        {
            ModMain mod = new ModMain();
            var employeerepo = new EmployeesRepo();
            if (EmployeeId != null)
            {
                //update
                var emp = employeerepo.GetEmployeeByID(EmployeeId);
                _Mapper.Map(this, emp);
                emp.Dept = mod.GetDepartmentByID(DeptId);
                emp.Unit = mod.GetUnitByID(UnitId);
                emp.Branch = mod.GetBranchByID(BranchId);
                emp.Grade = mod.GetGradeByID(GradeId.ToString());
                emp.Paysheet = mod.GetPaySheetByID(SheetId2.ToString());
                emp.PrimaryManager = mod.GetEmployeeNameByID(PrimaryManagerId);
                if (Image != null)
                {
                    var filename = $"{Guid.NewGuid()}_{Image.FileName}";
                    var filepath = Path.Combine(_environment.WebRootPath, "Uploads", "Profile", filename);
                   
                    using (var fileStream = new FileStream(filepath, FileMode.Create))
                    {
                        Image.CopyTo(fileStream);
                    }
                    emp.Image = filename;
                }
              
                int result = employeerepo.UpdateEmployee(emp, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return true;
                }
                else
                {
                    ErrMsg = mod.GetErrorMessage();
                    ErrMsgType = mod.GetErrMessageTypeError();
                    return false;
                }
            }

            else
            {

                //New Record

                Employees emp = new Employees();
                //map rest of data
                _Mapper.Map(this, emp);
                emp.OtherNames = OtherNames.ToUpper();
                emp.Surname = Surname.ToUpper();
                emp.Dept = mod.GetDepartmentByID(DeptId);
                emp.Unit = mod.GetUnitByID(UnitId);
                emp.Branch = mod.GetBranchByID(BranchId);
                emp.Grade = mod.GetGradeByID(GradeId.ToString());
                emp.EmployeeId = mod.GetEmployeeNo("EMPLOYEEID");
                emp.Paysheet = mod.GetPaySheetByID(SheetId2.ToString());
                emp.PrimaryManager = mod.GetEmployeeNameByID(PrimaryManagerId);
                //bind employeeid for redirection to gradelevel
                EmployeeId = emp.EmployeeId;
                if (Image != null)
                {
                    var filename = $"{Guid.NewGuid()}_{Image.FileName}";
                    var filepath = Path.Combine(_environment.WebRootPath, "Uploads", "Profile", filename);
                    using (var fileStream = new FileStream(filepath, FileMode.Create))
                    {
                        Image.CopyTo(fileStream);
                    }
                    emp.Image = filename;
                }
                           
                int result = employeerepo.CreateEmployee(emp, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    //save employee details in user
                    var userpass = mod.GetRandomString();
                    Users user = new Users();
                    user.EmployeeId = emp.EmployeeId;
                    user.LastName = emp.Surname.ToUpper();
                    user.Username = Username;
                    user.FirstName = OtherNames.ToUpper();
                    user.Phone = MobilePhone;
                    user.Email = Email;
                    user.Address = Address;
                    user.Password = mod.Encrypt(userpass);

                    int userrec = new UsersRepo().CreateUser(user, HttpContext.Session.GetString("Curruser"));
                   
                    //give the employee selfdservice role only by default
                    UserRoles role = new UserRoles();
                    role.UserId = userrec;
                    role.RoleId = (long)ModMain.ROLESIDS.SelfService;
                    int rolres = new UserRolesRepo().CreateUserRole(role, HttpContext.Session.GetString("Curruser"));
                    //Insert Promotion
                    Promotion p = new Promotion();
                    p.Othernames = emp.OtherNames.ToUpper();
                    p.Surname = emp.Surname.ToUpper();
                    p.Bs = emp.BasicSalary;
                    p.Ta = emp.TransportAllowance;
                    p.Ha = emp.HousingAllowance;
                    p.Ha = emp.HousingAllowance;
                    p.EmployeeId = emp.EmployeeId;
                    p.SheetId = (int)emp.SheetId2;
                    p.Grade = emp.Grade;
                    p.ProDate = emp.HireDate;
                    p.Ytax = emp.AnnualTax;
                    p.Step = emp.Step;
                    int promores = new PromotionRepo().CreatePromotion(p, HttpContext.Session.GetString("Curruser"));
                    if (rolres > 0 && userrec > 0 && promores > 0)
                    {
                        //send mail to employee email to login
                        var setting = new SettingsRepo().GetCompanySettings();
                        var fullname = $"{user.FirstName} {user.LastName}";
                        string body = mod.PopulateBodyEmployeeLogin(fullname, user.Username, userpass);
                        bool mailsuccessfull = mod.SendHtmlFormattedEmail(user.Email, "", "Login Details", body, setting, null);
                        //send mail to ict to give employee more permissions if needed.
                        TempData["success"] = true;
                        TempData["message"] = mod.GetSuccessMessage();
                        return true;
                    }

                    ErrMsg = mod.GetErrorMessage();
                    ErrMsgType = mod.GetErrMessageTypeError();
                    return false;

                }
                else
                {
                    ErrMsg = mod.GetErrorMessage();
                    ErrMsgType = mod.GetErrMessageTypeError();
                    return false;
                }


            }
        }
    }
}
