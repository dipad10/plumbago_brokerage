using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeeMedicalProcessingModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }
        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "hr_employee_medicalprocessing";
            TableList.Groupname = "hr_employee_medicalprocessing";
            TableList.MedicalsQueryable = new MedicalRepo().GetMedicalActivitiesBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("EmployeeMedicalProcessing", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option });
        }

        public IActionResult OnPostApprove(List<int> SelectedIDs)
        {
            ModMain mod = new ModMain();
            //var leaverepo = new LeaveRepo();
            //foreach (var item in SelectedIDs)
            //{
            //    leaverepo.DeleteLeaveByID(item);
            //}
            TempData["success"] = true;
            TempData["message"] = mod.GetDeletedMessage();
            return RedirectToPage("EmployeeMedicalProcessing");

        }
    }
}
