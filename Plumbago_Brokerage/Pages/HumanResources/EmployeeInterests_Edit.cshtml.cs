using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    [BindProperties]
    public class Interests_EditModel : PageModel
    {
        public long Id { get; set; }
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public decimal? A1 { get; set; }
        public string Remarks { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        private readonly IMapper _Mapper;

        public Interests_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id, int? detailid)
        {
            //for toptabs
            var HobbiesRepo = new HobbiesRepo();
            EditModel.Groupname = "hr_employees";
            EditModel.Editid = id.ToString();

            TableList.Groupname = "hr_interests_edit";
            TableList.Hobbies = HobbiesRepo.GetAllEmployeeHobbies(id.ToString());
            if (id != null)
            {
                //fetch employee top hobbies
                
                if (detailid != null)
                {
                   var hobbies = HobbiesRepo.GetHobbiesByEmployeeIDAndHobbyID(id.ToString(), (long)detailid);
                    _Mapper.Map(hobbies, this);
                    EditModel.PageTitle = $"Edit Interest for Employee: {id} and Detail: { detailid}";
                }
                else
                {
                    EditModel.PageTitle = $"Edit Interest for Employee: {id}";
                }

                return Page();
            }

            EditModel.PageTitle = "Add new Employee Interest";
         
            return Page();
        }
    }
}
