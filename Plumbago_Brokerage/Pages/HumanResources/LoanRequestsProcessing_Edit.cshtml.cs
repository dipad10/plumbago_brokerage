using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.HumanResources
{

    public class LoanRequestsProcessing_EditModel : PageModel
    {
        public long LoanId { get; set; }
        public string EmployeeId { get; set; }
        public string Surname { get; set; }
        public string Othernames { get; set; }
        public string TransactionType { get; set; }
        public long? DedId { get; set; }
        public string LoanName { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AmtPaid { get; set; }
        public decimal? TotalAmtDue { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? IntAmt { get; set; }
        public decimal? MonthlyDed { get; set; }
        public string Duration { get; set; }
        public DateTime? Tdate { get; set; }
        public string RejectionMessage { get; set; }
        public IEnumerable<SelectListItem> LoanTypes { get; set; }
        public IEnumerable<SelectListItem> LoanDuration { get; set; }
        public List<LoanDetails> loandetails { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }

        private readonly IMapper _Mapper;

        public LoanRequestsProcessing_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(long? id)
        {
          
            var mod = new ModMain();
            if (id != null)
            {

                var loan = new LoanRepo().GetLoanByID(id);
                _Mapper.Map(loan, this);
                loandetails = new LoanDetailsRepo().GetLoanDetailsByLoanID(id);
                EditModel.PageTitle = $"Edit Loan: {id}";
                LoanTypes = mod.PopulateLoanTypes();
                LoanDuration = mod.PopulateLoanDuration();
                return Page();
            }
            LoanTypes = mod.PopulateLoanTypes();
            LoanDuration = mod.PopulateLoanDuration();
            InterestRate = mod.GetDefaultInterestRate();
            EditModel.PageTitle = "Create new Loan request";
            return Page();
        }
    }
}
