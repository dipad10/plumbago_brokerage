using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class EmployeesModel : PageModel
    {


        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }


        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "hr_employees";
            TableList.Groupname = "hr_employees";
            Searchfilter.Branches = mod.PopulateBranches();
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            TableList.Employees = new EmployeesRepo().GetEmployeesBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("Employees", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet });
        }
    }
}
