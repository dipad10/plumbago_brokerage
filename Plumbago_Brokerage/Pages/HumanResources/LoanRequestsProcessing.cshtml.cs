using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.HumanResources
{
    public class LoanRequestsModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "hr_employee_loanprocessing";
            TableList.Groupname = "hr_employee_loanprocessing";
            TableList.Loans = new LoanRepo().GetLoansBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("LoanRequestsProcessing", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option, Searchfilter.Status });
        }

        public IActionResult OnPostApprove(List<int> SelectedIDs)
        {
            ModMain mod = new ModMain();
            var loanrepo = new LoanRepo();
            var loandetailrepo = new LoanDetailsRepo();
            var payslipdetailrepo = new PaySlipDetailRepo();
            bool Issuccess = false;
            if (SelectedIDs.Count <= 0)
            {
                ErrMsg = "Please select a loanrequest first";
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }

            foreach (var id in SelectedIDs)
            {
                var loanrec = loanrepo.GetLoanByID(id);
               var appres = loanrepo.ApproveLoan(loanrec, HttpContext.Session.GetString("Curruser"));
                var detailrec = loandetailrepo.GetLoanDetailsByLoanID(id);
                var Apprdetail = loandetailrepo.ApproveLoanDetails(detailrec, HttpContext.Session.GetString("Curruser"));
                var loandetail = loandetailrepo.GetLoanDetailsByPeriodLoanID(id, mod.GetLoanNextMonth(loanrec.StartDate));
                //insert into payslip details as deduction
                var payslipdetails = new PaySlipDetails();
                payslipdetails.Period = loandetail.Period;
                payslipdetails.EmployeeId = loanrec.EmployeeId;
                payslipdetails.Deductions = loandetail.LoanName.ToUpper();
                payslipdetails.DeductionAmount = loandetail.MonthLoanAmt;
                payslipdetails.DeductionType = "LOAN";
                var result = payslipdetailrepo.CreatePayslipDetails(payslipdetails, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    //send mail to employee approval
                    var emp = new EmployeesRepo().GetEmployeeByID(loanrec.EmployeeId);
                    var setting = new SettingsRepo().GetCompanySettings();
                    var fullname = $"{emp.OtherNames} {emp.Surname}";
                    string body = mod.PopulateBodyLoanApproved(fullname, loandetail.LoanName, mod.FormatNaira(loanrec.Amount), mod.FormatNaira(loanrec.TotalAmtDue), mod.FormatDate(loanrec.StartDate.Value), mod.FormatDate(loanrec.DueDate.Value), mod.FormatNaira(loandetail.MonthLoanAmt), appres.TransStatus );
                    mod.SendHtmlFormattedEmail(emp.Email, "", "Loan Request Approved!", body, setting, null);
                    Issuccess = true;
                }

            }
            if (Issuccess)
            {
                TempData["success"] = true;
                TempData["message"] = "Loan Approved Successfully";
                return RedirectToPage("LoanRequestsProcessing");
            }
            else
            {
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
           

        }
    }
}
