using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.GeneralSettings
{
    [BindProperties]
    public class DeductionRates_EditModel : PageModel
    {
        public long Hid { get; set; }
        public long DedId { get; set; }
        public string DedName { get; set; }
        public decimal? Value1 { get; set; }
        public decimal? Percentage { get; set; }
        public string Steps { get; set; }
        public long? GradeId { get; set; }
        public string Grade { get; set; }
        public int? SheetId { get; set; }
        public string Sheet { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        public string RateType { get; set; }
        public IEnumerable<SelectListItem> Paysheets { get; set; }
        public IEnumerable<SelectListItem> Deductions { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        private readonly IMapper _Mapper;

        public DeductionRates_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();
            //var setting = new SettingsRepo().GetCompanySettings();
            if (id != null)
            {

                var deductionrate = new PayDeductionsRepo().GetPayDeductionByID(id);
                _Mapper.Map(deductionrate, this);
                EditModel.PageTitle = $"Edit Deduction Rate: {id}";
                Deductions = mod.PopulateDeductionIDs();
                Paysheets = mod.PopulatePaySheets();
                return Page();
            }
            Deductions = mod.PopulateDeductionIDs();
            Paysheets = mod.PopulatePaySheets();
            EditModel.PageTitle = "Create new Paysheet Deduction Rate";
            return Page();

        }

        public IActionResult OnPostSave()
        {
            var mod = new ModMain();
            if (Hid > 0)
            {
                //update
                var rec = new PayDeductionsRepo().GetPayDeductionByID(Hid);
                _Mapper.Map(this, rec);
                if (RateType == "Percentage")
                {
                    rec.Percentage = Value1;
                    rec.Value1 = 0;
                }
                rec.Sheet = mod.GetPaySheetByID(SheetId.ToString());
                rec.DedName = mod.GetDeductionNameByID(DedId.ToString());
                var res = new PayDeductionsRepo().UpdatePayDeduction(rec, HttpContext.Session.GetString("Curruser"));
                if (res > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return RedirectToPage("DeductionRates");
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
            else
            {
                //new record
                var paydeductions = new PayDeductions();
                _Mapper.Map(this, paydeductions);

                //check if ratetype is percentage. if yes save percentage value and set fixed value to 0 else just save fixed value.
                if (RateType == "Percentage")
                {
                    paydeductions.Percentage = Value1;
                    paydeductions.Value1 = 0;
                }
                paydeductions.Sheet = mod.GetPaySheetByID(SheetId.ToString());
                paydeductions.DedName = mod.GetDeductionNameByID(DedId.ToString());
                var res = new PayDeductionsRepo().CreatePayDeduction(paydeductions, HttpContext.Session.GetString("Curruser"));
                if (res > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return RedirectToPage("DeductionRates");
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }


        }
    }
}
