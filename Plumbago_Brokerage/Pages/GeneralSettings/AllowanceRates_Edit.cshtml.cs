using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.GeneralSettings
{
    [BindProperties]
    public class AllowanceRates_EditModel : PageModel
    {
        public long Hid { get; set; }
        public long AllowId { get; set; }
        public string AllowName { get; set; }
        public decimal? Value1 { get; set; }
        public decimal? Percentage { get; set; } = 0;
        public string Steps { get; set; }
        public long? GradeId { get; set; }
        public string Grade { get; set; }
        public string Sheet { get; set; }
        public int? SheetId { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        public string RateType { get; set; }
        public IEnumerable<SelectListItem> Paysheets { get; set; }
        public IEnumerable<SelectListItem> Allowances { get; set; }
        public IEnumerable<PayAllowance> PayAllowances { get; set; }

        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        private readonly IMapper _Mapper;

        public AllowanceRates_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();
            var repo = new PayAllowanceRepo();
            //var setting = new SettingsRepo().GetCompanySettings();
            if (id != null)
            {

                var allowancerate = repo.GetPayAllowanceByID(id);
              
                _Mapper.Map(allowancerate, this);
                EditModel.PageTitle = $"Edit Allowance Rate: {id}";
                Allowances = mod.PopulateAllowanceIDs();
                Paysheets = mod.PopulatePaySheets();
                PayAllowances = repo.GetAllPayAllowances();
                return Page();
            }
            Allowances = mod.PopulateAllowanceIDs();
            Paysheets = mod.PopulatePaySheets();
            PayAllowances = repo.GetAllPayAllowances();
            EditModel.PageTitle = "Create new Allowance Rate";
            return Page();

        }

        public IActionResult OnPostSaveAndAddNew()
        {
            var mod = new ModMain();
            var repo = new PayAllowanceRepo();
            if (Hid > 0)
            {
                //update
                var rec = new PayAllowanceRepo().GetPayAllowanceByID(Hid);
                _Mapper.Map(this, rec);
                if (RateType == "Percentage")
                {
                    rec.Percentage = Value1;
                    rec.Value1 = 0;
                }
                rec.Sheet = mod.GetPaySheetByID(SheetId.ToString());
                rec.AllowName = mod.GetAllowanceNameByID(AllowId.ToString());

                var res = new PayAllowanceRepo().UpdatePayAllowance(rec, HttpContext.Session.GetString("Curruser"));
                if (res > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    Allowances = mod.PopulateAllowanceIDs();
                    Paysheets = mod.PopulatePaySheets();
                    PayAllowances = repo.GetAllPayAllowances();
                    EditModel.PageTitle = "Create new Allowance Rate";
                    return Page();
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
            else
            {
                //new record
                var payallowance = new PayAllowance();
                _Mapper.Map(this, payallowance);

                //check if ratetype is percentage. if yes save percentage value and set fixed value to 0 else just save fixed value.
                if (RateType == "Percentage")
                {
                    payallowance.Percentage = Value1;
                    payallowance.Value1 = 0;
                }
                payallowance.Sheet = mod.GetPaySheetByID(SheetId.ToString());
                payallowance.AllowName = mod.GetAllowanceNameByID(AllowId.ToString());
                var res = new PayAllowanceRepo().CreatePayAllowance(payallowance, HttpContext.Session.GetString("Curruser"));
                if (res > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    Allowances = mod.PopulateAllowanceIDs();
                    Paysheets = mod.PopulatePaySheets();
                    PayAllowances = repo.GetAllPayAllowances();
                    EditModel.PageTitle = "Create new Allowance Rate";
                    return Page();
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }

        
        }

        public IActionResult OnPostSubmit()
        {
            var mod = new ModMain();
            if (Hid > 0)
            {
                //update
                var rec = new PayAllowanceRepo().GetPayAllowanceByID(Hid);
                _Mapper.Map(this, rec);
                if (RateType == "Percentage")
                {
                    rec.Percentage = Value1;
                    rec.Value1 = 0;
                }
                rec.Sheet = mod.GetPaySheetByID(SheetId.ToString());
                rec.AllowName = mod.GetAllowanceNameByID(AllowId.ToString());

                var res = new PayAllowanceRepo().UpdatePayAllowance(rec, HttpContext.Session.GetString("Curruser"));
                if (res > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return RedirectToPage("AllowanceRates");
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
            else
            {
                //new record
                var payallowance = new PayAllowance();
                _Mapper.Map(this, payallowance);

                //check if ratetype is percentage. if yes save percentage value and set fixed value to 0 else just save fixed value.
                if (RateType == "Percentage")
                {
                    payallowance.Percentage = Value1;
                    payallowance.Value1 = 0;
                }
                payallowance.Sheet = mod.GetPaySheetByID(SheetId.ToString());
                payallowance.AllowName = mod.GetAllowanceNameByID(AllowId.ToString());
                var res = new PayAllowanceRepo().CreatePayAllowance(payallowance, HttpContext.Session.GetString("Curruser"));
                if (res > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return RedirectToPage("AllowanceRates");
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
        }
    }
}
