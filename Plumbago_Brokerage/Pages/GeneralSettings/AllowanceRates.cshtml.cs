using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.GeneralSettings
{
    public class AllowanceRatesModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }


        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "gs_allowancerates";
            TableList.Groupname = "gs_allowancerates";
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            TableList.payAllowances = new PayAllowanceRepo().GetPayAllowanceBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("AllowanceRates", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet });
        }
    }
}
