using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.GeneralSettings
{
    public class DeductionRatesModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }


        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "gs_deductionrates";
            TableList.Groupname = "gs_deductionrates";
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            TableList.payDeductions = new PayDeductionsRepo().GetPayDeductionBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("DeductionRates", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet });
        }
    }
}
