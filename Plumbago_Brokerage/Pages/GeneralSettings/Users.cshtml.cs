﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Plumbago_Brokerage.Data;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;

namespace Plumbago_Brokerage.Pages.GeneralSettings
{
    public class UsersModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }

        public override void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
                RedirectToPage("/Welcome/Login");
            }
        }

        public void OnGet(DateTime? From, DateTime? To, string Contains, string Option)
        {
            Searchfilter.Groupname = "adm_users";
            TableList.Groupname = "adm_users";
            TableList.Users = new UsersRepo().GetUsersBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("Users", new { Searchfilter.From, Searchfilter.To, Searchfilter.Contains, Searchfilter.Option });
        }
    }
}
