﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Plumbago_Brokerage.Data;
using Plumbago_Brokerage.Models;
using AutoMapper;
using Plumbago_Brokerage.Data.Repository;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Plumbago_Brokerage.Data.Global;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Plumbago_Brokerage.Pages.GeneralSettings
{
    [BindProperties]
    public class Users_editModel : PageModel
    {
        private readonly IMapper _Mapper;

        public Users_editModel(IMapper Mapper)
        {
            _Mapper = Mapper;          
        }

        #region Properties
        public long UserId { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Required]
        public string ConfirmPassword { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public DateTime? PwdExpiry { get; set; }
        [Required]
        public string SecQuestion { get; set; }
        [Required]
        public string SecAnswer { get; set; }
        [Required]
        public bool Active { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        #endregion

        public override void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
                RedirectToPage("/Welcome/Login");
            }
        }

        public IActionResult OnGet(long? id)
        {
            ModMain mod = new ModMain();
            if (id != null)
            {
                var user = new UsersRepo().GetUserByID(id);
                user.Password = mod.Decrypt(user.Password);
                ConfirmPassword = user.Password;
                _Mapper.Map(user, this);
                ViewData["pagetitle"] = $"User {id} edit";
            }
  
            ViewData["pagetitle"] = "Create new user";
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public IActionResult OnPostSave()
        {
           
            ModMain mod = new ModMain();
            if (!ModelState.IsValid) return Page();
            if (Password != ConfirmPassword) { ErrMsg = "Passwords do not Match. Please try again"; return Page(); }
            Password = mod.Encrypt(Password);
            if(UserId > 0)
            {
                //update
                var user = new UsersRepo().GetUserByID(UserId);
                _Mapper.Map(this, user);
                int result = new UsersRepo().UpdateUser(user, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return RedirectToPage("./Users");
                }
                else
                {
                    ErrMsg = mod.GetErrorMessage();
                    return Page();
                }
            }
            else
            {
                Users user = new Users();
                _Mapper.Map(this, user);
                int result = new UsersRepo().CreateUser(user, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return RedirectToPage("./Users");
                }
                else
                {
                    ErrMsg = mod.GetErrorMessage();
                    return Page();
                }


            }
          
        }

    }
}
