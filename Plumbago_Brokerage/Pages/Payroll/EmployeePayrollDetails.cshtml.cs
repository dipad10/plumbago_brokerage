using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.Payroll
{
    public class EmployeePayrollDetailsModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }
        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "payroll_EmployeePayroll_Details";
            TableList.Groupname = "payroll_EmployeePayroll_Details";
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            Searchfilter.Periods = mod.PopulatePeriodOld();
            TableList.Payslips = new PaySlipRepo().GetPaySlipsBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("EmployeePayrollDetails", new { Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet, Searchfilter.Period });
        }

        public IActionResult OnPostMailPayslip(List<int> SelectedIDs)
        {
            foreach (var rec in SelectedIDs)
            {

            }
            return Page();
        }
        public IActionResult OnPostPrintPayslip(List<int> SelectedIDs)
        {
            var rec = SelectedIDs[0];

            return Page();
        }
    }
}
