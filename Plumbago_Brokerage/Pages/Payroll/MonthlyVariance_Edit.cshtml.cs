using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.Payroll
{
    public class MonthlyVariance_EditModel : PageModel
    {
        public int Id { get; set; }
        public int? SheetId { get; set; }
        public string EmployeeId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string Period { get; set; }
        public decimal? Basic { get; set; }
        public decimal? Dtotal { get; set; }
        public decimal? NetPay { get; set; }
        public string Confirmation { get; set; }
        public string BranchId { get; set; }
        public string Branch { get; set; }
        public string DeptId { get; set; }
        public string UnitId { get; set; }
        public string Step { get; set; }
        public long? GradeId { get; set; }
        public string Grade { get; set; }
        public string EmployeeStatus { get; set; }
        public string Detail { get; set; }
        public string TransGuid { get; set; }
        public string TransStatus { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string BatchGuid { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Atotal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? GrossPay { get; set; }
        public decimal? HousingAllowance { get; set; }
        public decimal? TransportAllowance { get; set; }

        public List<PaySlipDetails> PayslipAllowances { get; set; }
        public List<PaySlipDetails> PayslipDeductions { get; set; }

        public IEnumerable<SelectListItem> Periods { get; set; }
        //public List<PayAllowance> Allowances { get; set; }
        //public List<PayDeductions> Deductions { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        private readonly IMapper _Mapper;

        public MonthlyVariance_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();
            var paysliprepo = new PaySlipRepo();
            var paydetailrepo = new PaySlipDetailRepo();
            if (id != null)
            {
                var slip = paysliprepo.GetPayslipByID(id);               
                _Mapper.Map(slip, this);
                //bind payslip details
                PayslipAllowances = paydetailrepo.GetPaySlipAllowancesByPayslipID((int)id);
                PayslipDeductions = paydetailrepo.GetPaySlipDeductionsByPayslipID((int)id);
                EditModel.PageTitle = $"Employee Monthly Variance Details: {id}";
                return Page();
            }
            TempData["error"] = true;
            TempData["message"] = "Select a Payslip first";
            return RedirectToPage("EmployeePayrollDetails");

        }
    }
}
