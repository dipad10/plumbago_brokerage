using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.Payroll
{
    public class Employee_PayrollModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "payroll_emp_payroll";
            TableList.Groupname = "payroll_emp_payroll";
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            TableList.Employees = new EmployeesRepo().GetEmployeesBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("EmployeePayroll", new { Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet });
        }
    }
}
