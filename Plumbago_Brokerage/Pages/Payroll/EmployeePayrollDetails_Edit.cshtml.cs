using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.Payroll
{
    [BindProperties]
    public class EmployeePayrollDetails_EditModel : PageModel
    {
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string Period { get; set; }
        public decimal? Basic { get; set; }
        public decimal? Dtotal { get; set; }
        public decimal? NetPay { get; set; }
  
        public string Grade { get; set; }
      
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Atotal { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? GrossPay { get; set; }
        public decimal? HousingAllowance { get; set; }
        public decimal? TransportAllowance { get; set; }

        public List<PaySlipDetails> PayslipAllowances { get; set; }
        public List<PaySlipDetails> PayslipDeductions { get; set; }

        public IEnumerable<SelectListItem> Periods { get; set; }
        //public List<PayAllowance> Allowances { get; set; }
        //public List<PayDeductions> Deductions { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        private readonly IMapper _Mapper;

        public EmployeePayrollDetails_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            var mod = new ModMain();
            var paysliprepo = new PaySlipRepo();
            var paydetailrepo = new PaySlipDetailRepo();
            if (id != null)
            {
                var slip = paysliprepo.GetPayslipByID(id);
                _Mapper.Map(slip, this);
                //bind payslip details
                PayslipAllowances = paydetailrepo.GetPaySlipAllowancesByPayslipID((int)id);
                PayslipDeductions = paydetailrepo.GetPaySlipDeductionsByPayslipID((int)id);
                EditModel.PageTitle = $"Employee Payroll Details: {id}";
                return Page();
            }
            TempData["error"] = true;
            TempData["message"] = "Select a Payslip first";
            return RedirectToPage("MonthlyVariance");

        }

        public IActionResult OnPostSave()
        {
            var mod = new ModMain();
            var paysliprepo = new PaySlipRepo();
            var paydetailrepo = new PaySlipDetailRepo();
            var slip = paysliprepo.GetPayslipByID(Id);
            var rec = _Mapper.Map(this, slip);
            rec.Atotal = PayslipAllowances.Sum(x => x.AllowanceAmount);
            var res = paysliprepo.UpdatePayslip(rec, HttpContext.Session.GetString("Curruser"));
            if (res > 0)
            {
                //update detail
                foreach (var allw in PayslipAllowances)
                {
                    var detail = paydetailrepo.GetPaySlipDetailByID(allw.Id);
                    int detailrs = 0;
                    detail.AllowanceAmount = allw.AllowanceAmount;
                    detailrs = new PaySlipDetailRepo().UpdatePayslipDetails(detail, HttpContext.Session.GetString("Curruser"));
                }

                foreach (var deduct in PayslipDeductions)
                {
                    var deductdetail = paydetailrepo.GetPaySlipDetailByID(deduct.Id);
                    int detailddrs = 0;
                    deductdetail.DeductionAmount = deduct.DeductionAmount;
                    detailddrs = new PaySlipDetailRepo().UpdatePayslipDetails(deductdetail, HttpContext.Session.GetString("Curruser"));
                }
                TempData["success"] = true;
                TempData["message"] = $"Payroll Details Updated for Payslip {Id}. Print the Payslip Here.";
                return RedirectToPage("EmployeePayrollDetails", new { Period });
            }
            else
            {
                ErrMsg = mod.GetErrorMessage(); ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }

        }
    }
}
