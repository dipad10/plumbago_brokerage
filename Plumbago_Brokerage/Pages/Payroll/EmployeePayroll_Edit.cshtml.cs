using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.Payroll
{
    [BindProperties]
    public class EmployeePayroll_EditModel : PageModel
    {
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string StaffIdno { get; set; }
        public string Title { get; set; }
        public string Username { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Sex { get; set; }
        public DateTime? BirthDate { get; set; }
        public string MaritalStatus { get; set; }
        public string StateOrigin { get; set; }
        public string NationalIdno { get; set; }
        public string AcctNo1 { get; set; }
        public string AcctName1 { get; set; }
        public string AcctNo2 { get; set; }
        public string AcctName2 { get; set; }
        public string BranchId { get; set; }
        public string Branch { get; set; }
        public string DeptId { get; set; }
        public string Paysheet { get; set; }
        public string Dept { get; set; }
        public string PrimaryManagerId { get; set; }
        public string PrimaryManager { get; set; }
        public string UnitId { get; set; }
        public string Unit { get; set; }
        public long? GradeId { get; set; }
        public string Grade { get; set; }
        public DateTime? HireDate { get; set; }
        public string Image { get; set; }
        public string Telephone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string NextKin { get; set; }
        public string KinAddress { get; set; }
        public string KinPhone { get; set; }
        public string KinRelationship { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public long? Smoker { get; set; }
        public string DisableType { get; set; }
        public string Remarks { get; set; }
        public string Tag { get; set; }
        public byte[] Photo { get; set; }
        public string PayFirstMonth { get; set; }
        public long? SheetId2 { get; set; }
        public string ConfirmStatus { get; set; }
        public long? ConfirmDuration { get; set; }
        public DateTime? ConfirmationDate { get; set; }
        public DateTime? RetiredDate { get; set; }
        public byte? Deleted { get; set; }
        public byte? Active { get; set; }
        public string Submitby { get; set; }
        public DateTime? SubmitOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Hmoname { get; set; }
        public string Hmoid { get; set; }
        public string Email2 { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? BasicSalary { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? TransportAllowance { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? HousingAllowance { get; set; }
        public decimal? AnnualTax { get; set; }
        public string Step { get; set; }
        public string Period { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Dtotal { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? NetPay { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? GrossPay { get; set; } = 0;
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        //public decimal? DedAmounts { get; set; } = 0;
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        //public decimal? AllowAmounts { get; set; } = 0;

        public List<PaySlipDetails> PayslipDetails { get; set; }
     
        public IEnumerable<SelectListItem> Periods { get; set; }
        public List<PayAllowance> Allowances { get; set; }
        public List<PayDeductions> Deductions { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        private readonly IMapper _Mapper;

        public EmployeePayroll_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet(int? id)
        {
            
              var mod = new ModMain();          
            //for toptabs
            if (id != null)
            {
               var EmployeeInfo = new EmployeesRepo().GetEmployeeByID(id.ToString());
                if (EmployeeInfo.BasicSalary != null)
                {
                    EmployeeInfo.BasicSalary /= 12;
                }
                if (EmployeeInfo.HousingAllowance != null)
                {
                    EmployeeInfo.HousingAllowance /= 12;
                }
                if (EmployeeInfo.TransportAllowance != null)
                {
                    EmployeeInfo.TransportAllowance /= 12;
                }
                _Mapper.Map(EmployeeInfo, this);
                 EditModel.PageTitle = $"Payroll for Employee: {OtherNames} {Surname}";
                Periods = mod.PopulatePeriod();
                Allowances = new PayAllowanceRepo().GetAllowanceByPaysheet((int)SheetId2, BasicSalary);
                Deductions = new PayDeductionsRepo().GetDeductionsByPaysheet((int)SheetId2, BasicSalary);
                return Page();
            }
            TempData["error"] = true;
            TempData["message"] = "Select an Employee first";
            return RedirectToPage("EmployeePayroll");
        }

   

        public IActionResult OnPostSave()
        {
            var mod = new ModMain();
            if (EmployeeId == null)
            {
                TempData["error"] = true;
                TempData["message"] = "Select an Employee first";
                return RedirectToPage("EmployeePayroll");
            }
            //insert payslip
            if (mod.IsPayrollAlreadyDoneForEmployee(EmployeeId, Period) == true)
            {
                ErrMsg = "Payroll has already been done for this employee for the selected month. Please select a new month or do monthly variance"; ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
            var emp = new EmployeesRepo().GetEmployeeByID(EmployeeId.ToString());
            var payslip = new PaySlips();
            payslip.EmployeeId = EmployeeId;
            payslip.BranchId = emp.BranchId;
            payslip.Branch = emp.Branch;
            payslip.DeptId = emp.DeptId;
            payslip.Grade = emp.Grade;
            payslip.GradeId = emp.GradeId;
            payslip.OtherNames = emp.OtherNames;
            payslip.Surname = emp.Surname;
            payslip.HousingAllowance = HousingAllowance;
            payslip.TransportAllowance = TransportAllowance;
            payslip.Atotal = Allowances.Sum(x => x.Value1);
            payslip.Dtotal = Deductions.Sum(x => x.Value1);
            payslip.SheetId = (int)emp.SheetId2;
            payslip.GrossPay = GrossPay;
            payslip.Basic = BasicSalary;
            payslip.NetPay = NetPay;
            payslip.Confirmation = "CONFIRM";
            payslip.Period = Period;
            payslip.Step = emp.Step;
            payslip.UnitId = emp.UnitId;
            payslip.Step = emp.Step;
            payslip.TransStatus = "CONFIRM";

            payslip.EmployeeStatus = emp.ConfirmStatus;
            payslip.TransGuid = Guid.NewGuid().ToString();
            int res = new PaySlipRepo().CreatePaySlip(payslip, HttpContext.Session.GetString("Curruser"));
            if (res > 0)
            {
                //save payslipdetails
                
                foreach (var allw in Allowances)
                {
                    var detail = new PaySlipDetails();
                    int detailrs = 0;
                    detail.PaySlipId = res;
                    detail.Period = Period;
                    detail.PayslipGuid = payslip.TransGuid;
                    detail.AllowId = allw.AllowId;
                    detail.Allowance = allw.AllowName;
                    detail.AllowanceAmount = allw.Value1;
                    detail.EmployeeId = EmployeeId;
                    detailrs = new PaySlipDetailRepo().CreatePayslipDetails(detail, HttpContext.Session.GetString("Curruser"));
                }

                foreach (var ded in Deductions)
                {
                    var detailded = new PaySlipDetails();
                    int dedrs = 0;
                    detailded.PaySlipId = res;
                    detailded.Period = Period;
                    detailded.PayslipGuid = payslip.TransGuid;
                    //detailded.ded = ded.DedId;
                    detailded.Deductions = ded.DedName;
                    detailded.DeductionAmount = ded.Value1;
                    detailded.EmployeeId = EmployeeId;
                    dedrs = new PaySlipDetailRepo().CreatePayslipDetails(detailded, HttpContext.Session.GetString("Curruser"));
                }
                TempData["success"] = true;
                TempData["message"] = $"Payroll Successfully done for employee {EmployeeId}. Print the Payslip Here.";
                return RedirectToPage("EmployeePayrollDetails", new { Period });
            }

            else
            {
                ErrMsg = mod.GetErrorMessage(); ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }

           


        }
    }
}
