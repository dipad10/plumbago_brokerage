using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.Payroll
{
    [BindProperties]
    public class BatchPayrollProcessingModel : PageModel
    {
        public int Id { get; set; }
        public int? SheetId { get; set; }
        public string EmployeeId { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string Period { get; set; }
        public string Previousmonth { get; set; }
        public bool MailEmployees { get; set; }
        public bool IncludeExgratial { get; set; }
        public decimal? Basic { get; set; }
        public decimal? Dtotal { get; set; }
        public decimal? NetPay { get; set; }
        public string Confirmation { get; set; }
        public string BranchId { get; set; }
        public string DeptId { get; set; }
        public string UnitId { get; set; }
        public string Step { get; set; }
        public long? GradeId { get; set; }
        public string Grade { get; set; }
        public string EmployeeStatus { get; set; }
        public string Detail { get; set; }
        public string TransGuid { get; set; }
        public string TransStatus { get; set; }
        public IEnumerable<SelectListItem> Previousmonths { get; set; }
        public IEnumerable<SelectListItem> NextMonths { get; set; }
        public IEnumerable<SelectListItem> PaySheetGroups { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        private readonly IMapper _Mapper;

        public BatchPayrollProcessingModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }
        public IActionResult OnGet()
        {
            var mod = new ModMain();
            Previousmonths = mod.PopulatePeriod();
            NextMonths = mod.PopulateNextMonths();
            PaySheetGroups = mod.PopulatePaySheets();
            EditModel.PageTitle = $"Process Batch Payroll";
            return Page();
        }

        public IActionResult OnPostProcess()
        {
            var mod = new ModMain();
            var paysliprepo = new PaySlipRepo();
            var employeerepo = new EmployeesRepo();
            var curruser = HttpContext.Session.GetString("Curruser");
            string exgratial = IncludeExgratial ? "Y" : "N";
                var result = paysliprepo.ProcessBatchPayroll(Guid.NewGuid().ToString(), Previousmonth, Period, SheetId, curruser, exgratial);
                if (result > 0)
                {
                    if (MailEmployees == true)
                    {
                        //mail employee payslips
                        var payslips = paysliprepo.GetNextMonthPayslips(Period, SheetId);
                        if (mod.ProcessEmployeePayslipToEmail(payslips) == true)
                        {
                            TempData["success"] = true;
                            TempData["message"] = $"Batch Payroll Processed Successfuly with total: {result} records and Mail sent!";
                            return RedirectToPage("EmployeePayrollDetails", new { Period });
                        }
                        else
                        {
                            //mail not sent properly
                            TempData["success"] = true;
                            TempData["message"] = $"Batch Payroll Processed Successfuly with total: {result} records but an error occured while sending Employee mails. Contact your Administrator";
                            return RedirectToPage("EmployeePayrollDetails", new { Period });
                        }
                        
                    }
                    
                }
                ErrMsg = mod.GetErrorMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
       
        }

        public IActionResult OnPostEmail()
        {
            var mod = new ModMain();
            var setting = new SettingsRepo().GetCompanySettings();
           
            return Page();
        }
    }

    
}
