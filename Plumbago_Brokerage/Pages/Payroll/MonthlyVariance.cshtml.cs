using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;


namespace Plumbago_Brokerage.Pages.Payroll
{
    public class MonthlyVarianceModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public void OnGet()
        {
            var mod = new ModMain();
            Searchfilter.Groupname = "payroll_monthly_variance";
            TableList.Groupname = "payroll_monthly_variance";
            Searchfilter.Paysheets = mod.PopulatePaySheets();
            Searchfilter.Periods = mod.PopulatePeriodOld();
            TableList.Payslips = new PaySlipRepo().GetPaySlipsBySearchFilter(Searchfilter);
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("MonthlyVariance", new { Searchfilter.Contains, Searchfilter.Option, Searchfilter.Paysheet, Searchfilter.Period });
        }
    }
}
