using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Plumbago_Brokerage.Pages.Welcome
{
    public class LoginModel : PageModel
    {
        [BindProperty]
        [Required]
        public string Username { get; set; }
        [Required]
        [BindProperty]
        public string Password { get; set; }

        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }



        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid) return Page();

            ModMain mod = new ModMain();
            UsersRepo _UsersRepo = new UsersRepo();
            if (!_UsersRepo.IsUserExists(Username, mod.Encrypt(Password)))
            {
                ErrMsg = mod.GetInvalidLoginMessage();
                ErrMsgType = mod.GetErrMessageTypeError();
                return Page();
            }
            else
            {
                HttpContext.Session.SetString("Curruser", Username);
                var userroles = new RolesRepo();
                var user = _UsersRepo.GetUserByUsername(Username);
                HttpContext.Session.SetInt32("Curruserid", (int)user.UserId);
                HttpContext.Session.SetString("Employeeid", user.EmployeeId);
                var returnurl = Request.Query["ReturnUrl"];
                //if return url is empty.means its a new login so do normal process else redirect back to return url
                if (returnurl == "" || returnurl == "/")
                {
                    if (userroles.HasRole(userroles.GeneralSettings, user.UserId))
                    {
                        return RedirectToPage("/Welcome/Dashboard");
                    }
                    else
                    {
                        return RedirectToPage("/SelfService/Home");
                    }
                }
                else { return RedirectToPage(returnurl); }
               

              
            }

        }
    }
}
