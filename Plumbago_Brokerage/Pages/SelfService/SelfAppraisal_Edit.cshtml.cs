using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.SelfService
{
    public class SelfAppraisal_EditModel : PageModel
    {
        public int AppraisalId { get; set; }
        public string EmployeeId { get; set; }
        public string Surname { get; set; }
        public string Othernames { get; set; }
        public string Reviewer { get; set; }
        public string ReviewPeriod { get; set; }
        public DateTime? ReviewDate { get; set; }
        public IEnumerable<SelectListItem> Reviewperiods { get; set; }
        public IEnumerable<SelectListItem> Appraisalscores { get; set; }
        public List<AppraisalGrades> Appraisalgrades { get; set; }
        public List<AppraisalDetails> appraisalDetails { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        private readonly IMapper _Mapper;

      

        public SelfAppraisal_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
          
        }

        public IActionResult OnGet()
        {
            EditModel.Groupname = "selfservice_tab";
            var empid = HttpContext.Session.GetString("Employeeid");
            var employee = new EmployeesRepo().GetEmployeeByID(empid);
            Reviewer = employee.PrimaryManager;
            var mod = new ModMain();
            Reviewperiods = mod.PopulateNextMonths();
            Appraisalgrades = new AppraisalGradesRepo().GetAppraisalGrades();
            Appraisalscores = mod.PopulateAppraisalscores();
            EditModel.PageTitle = "Create new Self-Appraisal request";
            return Page();
        }

        public IActionResult OnPostSave()
        {           
                return Page();
           
        }
    }
}
