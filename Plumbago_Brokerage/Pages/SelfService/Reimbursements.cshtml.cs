using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    public class ReimbursementsModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public string ErrMsg { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }

        public void OnGet()
        {
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));

            TableList.Groupname = "selfservice_reimbursements";
            //set searchfilter to search by employeeid only          
            TableList.Reimbursements = new ExpensesRepo().GetAllExpensesByEmployeeID(user.EmployeeId);
        }

        public IActionResult OnPostDelete(List<int> SelectedIDs)
        {
            ModMain mod = new ModMain();
            var expenserepo = new ExpensesRepo();
            foreach (var item in SelectedIDs)
            {
                expenserepo.DeleteExpensesByID(item);
            }
            TempData["success"] = true;
            TempData["message"] = mod.GetDeletedMessage();
            return RedirectToPage("Reimbursements");

        }
    }
}
