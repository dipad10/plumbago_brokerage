using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    public class TrainingNeedsModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public string ErrMsg { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }
        public IActionResult OnGet()
        {
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));

            TableList.Groupname = "selfservice_trainingneeds";
            //set searchfilter to search by employeeid only          
            TableList.TrainingNeeds = new TrainingNeedsRepo().GetTrainingNeedsByEmployeeID(user.EmployeeId);
            return Page();
        }
    }
}
