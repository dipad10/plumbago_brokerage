using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    [BindProperties]
    public class Reimbursement_editModel : PageModel
    {
        public long ExpenseId { get; set; }
        [Required]
        public string ExpenseType { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Amount { get; set; } = 0;
        [Required]
        public DateTime? DateOfExpense { get; set; }
        [Required]
        public string Description { get; set; }
        public IEnumerable<SelectListItem> ExpenseTypes { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }

        public IFormFile ExpenseImages { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }

        private readonly IMapper _Mapper;
       
        private readonly IWebHostEnvironment _environment;

        public Reimbursement_editModel(IMapper Mapper, IWebHostEnvironment environment)
        {
            _Mapper = Mapper;
            _environment = environment;
        }

        public IActionResult OnGet(long? id)
        {
            EditModel.Groupname = "selfservice_tab";
          
            var mod = new ModMain();
            if (id != null)
            {

                var expense = new ExpensesRepo().GetExpensesByID(id);
                _Mapper.Map(expense, this);
                EditModel.PageTitle = $"Edit Reimbursement: {id}";
                ExpenseTypes = mod.PopulateExpenseTypes();
                return Page();
            }
            ExpenseTypes = mod.PopulateExpenseTypes();
            EditModel.PageTitle = "Create new reimbursement request";
            return Page();
        }

        public IActionResult OnPostSave()
        {
            if (!ModelState.IsValid) return Page();
            if (DoInsert())
            {
                return RedirectToPage("Reimbursements");
            }
            else
            {
                return Page();
            }
        }




        private bool DoInsert()
        {
            ModMain mod = new ModMain();
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));

            if (ExpenseId > 0)
            {
                //update
                var expense = new ExpensesRepo().GetExpensesByID(ExpenseId);

                _Mapper.Map(this, expense);
                int result = new ExpensesRepo().UpdateExpense(expense, user.Username);
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return true;
                }
                else
                {
                    EditModel.ErrMsg = mod.GetErrorMessage();
                    return false;
                }
            }
            else
            {
                //createnew
                Expenses expense = new Expenses();
                expense.EmployeeId = user.EmployeeId;
                expense.Surname = user.LastName;
                expense.Othernames = user.FirstName;
                _Mapper.Map(this, expense);
                int result = new ExpensesRepo().CreateExpense(expense, user.Username);
                if (result > 0)
                {
                    //save expenseimage
                    if (ExpenseImages != null)
                    {
                        var file = Path.Combine(_environment.ContentRootPath, "wwwroot", "Uploads", $"{Guid.NewGuid()}_{ExpenseImages.FileName}");
                        using (var fileStream = new FileStream(file, FileMode.Create))
                        {
                            ExpenseImages.CopyTo(fileStream);
                        }

                        ExpenseImages images = new ExpenseImages();
                        images.EmployeeId = user.EmployeeId;
                        images.ExpenseId = expense.ExpenseId;
                        images.ImagePath = file;
                        int expenseimageresult = new ExpenseImagesRepo().CreateExpenseImage(images, user.Username);
                        if (expenseimageresult > 0)
                        {
                            TempData["success"] = true;
                            TempData["message"] = mod.GetSuccessMessage();
                            return true;
                        }
                        else
                        {
                            ErrMsg = mod.GetErrorMessage();
                            ErrMsgType = mod.GetErrMessageTypeError();
                            return false;
                        }
                    }
                    return true;
                  
                }
                else
                {
                    EditModel.ErrMsg = mod.GetErrorMessage();
                    return false;
                }


            }
        }
    }
}
