﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Plumbago_Brokerage.Data;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    public class Leave_requestsModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public string ErrMsg { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }

        public override void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
                Response.Redirect("/Welcome/Login");
            }
        }

        public void OnGet()
        {
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));

            TableList.Groupname = "selfservice_leave";
            //set searchfilter to search by employeeid only          
            TableList.Leave = new LeaveRepo().GetAllLeaveByEmployeeID(user.EmployeeId);
        }

        public IActionResult OnPostDelete(List<int> SelectedIDs)
        {
            ModMain mod = new ModMain();
            var leaverepo = new LeaveRepo();
            foreach (var item in SelectedIDs)
            {
                leaverepo.DeleteLeaveByID(item);
            }
            TempData["success"] = true;
            TempData["message"] = mod.GetDeletedMessage();
            return RedirectToPage("Leave_requests");
          
        }
    }
}
