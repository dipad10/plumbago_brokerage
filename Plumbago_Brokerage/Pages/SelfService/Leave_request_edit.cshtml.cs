using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    [BindProperties]
    public class Leave_request_editModel : PageModel
    {
        public int LeaveId { get; set; }
        [Required]
        public string LeaveType { get; set; }
        [Required]
        public DateTime StartDate { get; set; } = DateTime.Now;
        [Required]
        public DateTime EndDate { get; set; } = DateTime.Now;
        public string Purpose { get; set; }

        public IEnumerable<SelectListItem> LeaveTypes { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }

        private readonly IMapper _Mapper;

        public Leave_request_editModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }


        public IActionResult OnGet(int? id)
        {
            EditModel.Groupname = "selfservice_tab";
           
            var mod = new ModMain();
            if (id != null)
            {
              
                var leave = new LeaveRepo().GetLeaveByID(id);               
                _Mapper.Map(leave, this);
                EditModel.IsDisabled = true;
               EditModel.PageTitle = $"Edit Leave: {id}";
                LeaveTypes = mod.PopulateLeaveTypes();
                return Page();
            }
            EditModel.IsDisabled = false;
            LeaveTypes = mod.PopulateLeaveTypes();
            EditModel.PageTitle = "Create new leave request";
            return Page();
        }


        public IActionResult OnPostSave()
        {
            if (!ModelState.IsValid) return Page();
            if (DoInsert())
            {
                return RedirectToPage("Leave_requests");
            }
            else
            {
                return Page();
            }

        }

        private bool DoInsert()
        {
            ModMain mod = new ModMain();
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));
         
            if (LeaveId > 0)
            {
                //update
                var leave = new LeaveRepo().GetLeaveByID(LeaveId);
                leave.Duration = mod.GetDaysBetween2Dates(StartDate, EndDate);
                _Mapper.Map(this, leave);
                int result = new LeaveRepo().UpdateLeave(leave, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return true;
                }
                else
                {
                    EditModel.ErrMsg = mod.GetErrorMessage();
                    return false;
                }
            }
            else
            {
                Leave leave = new Leave();
                leave.EmployeeId = user.EmployeeId;
                leave.Surname = user.LastName;
                leave.Othernames = user.FirstName;
                leave.LeaveArrears = 0;
                leave.TransStatus = mod.GetStatusPending();
                leave.Duration = mod.GetDaysBetween2Dates(StartDate, EndDate);
                _Mapper.Map(this, leave);
                int result = new LeaveRepo().CreateLeave(leave, HttpContext.Session.GetString("Curruser"));
                if (result > 0)
                {
                    TempData["success"] = true;
                    TempData["message"] = mod.GetSuccessMessage();
                    return true;
                }
                else
                {
                    EditModel.ErrMsg = mod.GetErrorMessage();
                    return false;
                }


            }
        }

    }
}
