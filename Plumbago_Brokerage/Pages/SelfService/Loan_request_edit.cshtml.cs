using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.VisualBasic;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using PlumbagoHR.Data.Repository;

namespace Plumbago_Brokerage.Pages.SelfService
{
    [BindProperties]
    public class Loan_request_editModel : PageModel
    {

        public long LoanId { get; set; }
        public string LoanName { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? Amount { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? TotalAmtDue { get; set; } = 0;

        public DateTime? StartDate { get; set; } = DateTime.Now;
        public DateTime? DueDate { get; set; } = DateTime.Now;
        public decimal? InterestRate { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? IntAmt { get; set; } = 0;
        public IEnumerable<SelectListItem> LoanTypes { get; set; }
        public IEnumerable<SelectListItem> LoanDuration { get; set; }
        public List<LoanDetails> loandetails { get; set; }
        public string Duration { get; set; }
        //public List<LoanDetails> loandetails { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }

        private readonly IMapper _Mapper;

        public Loan_request_editModel(IMapper Mapper)
        {
            _Mapper = Mapper;
        }

     
        public IActionResult OnGet(long? id)
        {
            EditModel.Groupname = "selfservice_tab";
           
            var mod = new ModMain();
            if (id != null)
            {
                
                var loan = new LoanRepo().GetLoanByID(id);
                loandetails = new LoanDetailsRepo().GetLoanDetailsByLoanID(id);
                _Mapper.Map(loan, this);
                EditModel.PageTitle = $"Edit Loan: {id}";
                LoanTypes = mod.PopulateLoanTypes();
                LoanDuration = mod.PopulateLoanDuration();
                return Page();
            }
            LoanTypes = mod.PopulateLoanTypes();
            LoanDuration = mod.PopulateLoanDuration();
            InterestRate = mod.GetDefaultInterestRate();
            EditModel.PageTitle = "Create new Loan request";
            return Page();
        }

        public IActionResult OnPostSave()
        {
            //
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));
            var employee = new EmployeesRepo().GetEmployeeByID(user.EmployeeId);
            var loanrepo = new LoanRepo();
            var loandetailrepo = new LoanDetailsRepo();
            var mod = new ModMain();
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //if (loanrepo.EmployeeHasLoan(user.EmployeeId) == true)
            //{
            //    ErrMsgType = mod.GetErrMessageTypeError();
            //    ErrMsg = $"You still have an active loan. You can't request another loan right now.";
            //    return Page();
            //}

            //if (loanrepo.EmployeeLessThanThreeYears(user.EmployeeId) == true)
            //{
            //    ErrMsgType = mod.GetErrMessageTypeError();
            //    ErrMsg = $"Sorry, You can't request a loan until you have spent at least three years with this company.";
            //    return Page();
            //}

            //Do processing

            decimal a = Convert.ToDecimal(InterestRate / 100);
            decimal j = Convert.ToDecimal(Duration);
            decimal k = Convert.ToDecimal(Amount);
            decimal g = Convert.ToDecimal(a / 12);
            decimal IntAmt;
            decimal NetLoanAmt;
            decimal PrevNetLoanAmt;
            decimal NetAmount = 0;
            decimal MonthLoanAmt;
            DateTime MonthDate;
            DateTime MonthDate2 = StartDate.Value.AddMonths(1);
            int DR;
            int duration = Convert.ToInt32(Duration);
            decimal NetAmountConst = Convert.ToDecimal(Financial.Pmt((double)(a / 12), (double)j, (double)-k));
            for (int i = 0; i < Convert.ToInt32(Duration); i++)
            {
                switch (i)
                {
                    case 0:
                        NetLoanAmt = (decimal)Amount;
                        MonthDate = StartDate.Value;
                        IntAmt = 0;
                        MonthLoanAmt = 0;
                        NetAmount = 0;
                        DR = duration;
                        HttpContext.Session.SetInt32("Prevloanamt", (int)NetLoanAmt);
                        break;
                    case 1:
                        PrevNetLoanAmt = (decimal)HttpContext.Session.GetInt32("Prevloanamt");
                        IntAmt = PrevNetLoanAmt * g;
                        NetAmount = NetAmountConst;
                        MonthLoanAmt = NetAmountConst - IntAmt;
                        DR = duration;
                        MonthDate = MonthDate2.AddMonths(i - 1);
                        NetLoanAmt = PrevNetLoanAmt - MonthLoanAmt;
                        HttpContext.Session.SetInt32("Prevloanamt", (int)NetLoanAmt);
                        break;
                    default:
                        PrevNetLoanAmt = (decimal)HttpContext.Session.GetInt32("Prevloanamt");
                        IntAmt = PrevNetLoanAmt * g;
                        NetAmount = NetAmountConst;
                        MonthLoanAmt = NetAmountConst - IntAmt;
                        DR = duration - (i - 1);
                        MonthDate = MonthDate2.AddMonths(i - 1);
                        NetLoanAmt = PrevNetLoanAmt - MonthLoanAmt;
                        HttpContext.Session.SetInt32("Prevloanamt", (int)NetLoanAmt);
                        break;

                }

                loandetails.Add(new LoanDetails()
                {
                    Sn = i + 1,
                    DurationRemaining = DR,
                    NetLoanAmt = NetLoanAmt,
                    IntAmt = IntAmt,
                    NetAmount = NetAmount,
                    PrincipalAmt = MonthLoanAmt,
                    MonthLoanAmt = NetAmount,
                    MonthDate = MonthDate,
                    Period = MonthDate.ToString("MMMM yyyy"),
                    Duration = duration,
                    LoanAmount = Amount,
                    StartDate = StartDate,
                    IntRate = InterestRate,
                    TransStatus = mod.GetStatusPending(),
                    LoanName = LoanName,
                    LoanType = "LOAN"
                });

            }

            Loans loan = new Loans();
            _Mapper.Map(this, loan);
            loan.Surname = employee.Surname;
            loan.Othernames = employee.OtherNames;
            loan.EmployeeId = user.EmployeeId;
            loan.TransactionType = "LOAN";
            loan.MonthlyDed = loandetails[0].MonthLoanAmt;
            loan.AmtPaid = 0;
            loan.DueDate = StartDate.Value.AddMonths(duration);
            loan.TransStatus = mod.GetStatusPending();
            var loanid = loanrepo.CreateLoan(loan, user.Username);
            if (loanid > 0)
            {
                //insert details
                var detailguid = Guid.NewGuid();
                int rs = 0;
                foreach (var item in loandetails)
                {
                    item.LoanId = loanid;
                    item.EmployeeId = loan.EmployeeId;
                    item.TransGuid = detailguid.ToString();          
                    rs = loandetailrepo.CreateLoanDetails(item, user.Username);
                }
                if (rs > 0)
                {
                    //details saved successfully send mail to employee
                    var setting = new SettingsRepo().GetCompanySettings();
                    var fullname = $"{employee.OtherNames} {employee.Surname}";
                    string body = mod.PopulateBodyLoanRaised(fullname, loan.LoanName, mod.FormatNaira(loan.Amount), loan.InterestRate.ToString(), mod.FormatNaira(loan.IntAmt), mod.FormatNaira(loan.TotalAmtDue), mod.FormatDate(loan.StartDate.Value), mod.FormatDate(loan.DueDate.Value), mod.GetStatusPending());
                    mod.SendHtmlFormattedEmail(employee.Email, "", "Your Loan Request", body, setting, null);
                    //Send mail to HR
                    var HRemployees = mod.GetAllHREmployeesUnit();
                    if (HRemployees.Count > 0)
                    {
                        foreach (var em in HRemployees)
                        {
                            var name = $"{em.OtherNames} {em.Surname}";
                            string body2 = mod.PopulateBodyLoanRaisedHR(name, fullname, loan.LoanName, mod.FormatNaira(loan.Amount), loan.InterestRate.ToString(), mod.FormatNaira(loan.IntAmt.ToString()), loan.TotalAmtDue.ToString(), mod.FormatDate(loan.StartDate.Value), mod.FormatDate(loan.DueDate.Value), mod.GetStatusPending());
                            mod.SendHtmlFormattedEmail(em.Email, "", "Loan Request Raised!", body2, setting, null);
                        }
                    }
                }
                else { ErrMsg = mod.GetErrorMessage(); ErrMsgType = mod.GetErrMessageTypeError(); return Page(); }
                               
                TempData["success"] = true;
                TempData["message"] = mod.GetSuccessMessage();
                return RedirectToPage("Loan_requests");
            }
            ErrMsg = mod.GetErrorMessage(); ErrMsgType = mod.GetErrMessageTypeError();
            return Page();
           
        }

        
    }
}
