using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    public class HomeModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public SearchFilterModel Searchfilter { get; set; }
        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public override void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
                RedirectToPage("/Welcome/Login");
            }
        }
        public void OnGet()
        {
           var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));

            TableList.Groupname = "selfservice_dashboard_leave";
            TableList.Groupname2 = "selfservice_dashboard_loan"; 
            TableList.Groupname3 = "selfservice_dashboard_reimbursements";
            //set searchfilter to search by employeeid only
            TableList.Loans = new LoanRepo().GetAllLoanByEmployeeID(user.EmployeeId);
            TableList.Leave = new LeaveRepo().GetAllLeaveByEmployeeID(user.EmployeeId);
            TableList.Reimbursements = new ExpensesRepo().GetAllExpensesByEmployeeID(user.EmployeeId);
        }
    }
}
