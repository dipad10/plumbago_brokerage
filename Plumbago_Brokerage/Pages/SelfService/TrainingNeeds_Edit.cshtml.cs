using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    [BindProperties]
    public class TrainingNeeds_EditModel : PageModel
    {
        public int TrainHisId { get; set; }
        public string EmployeeId { get; set; }
        public DateTime? DueDate { get; set; }
        public string TrainDesc { get; set; }
        public long? Duration { get; set; }
        public string Frequency { get; set; }
        public string TrainCadreId { get; set; }
        public string TrainCadre { get; set; }
        public string TrainVenue { get; set; }
        public string Organisers { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? CourseCost { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? AccomCost { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? TranspCost { get; set; } = 0;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n}")]
        public decimal? FeedingCost { get; set; } = 0;
        public DateTime? ReimbDate { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public string Remarks { get; set; }
        public string CertObtained { get; set; }
        public IEnumerable<SelectListItem> TrainingCadres { get; set; }
        [BindProperty(SupportsGet = true)]
        public PageEditModel EditModel { get; set; }


        private readonly IMapper _Mapper;
        public TrainingNeeds_EditModel(IMapper Mapper)
        {
            _Mapper = Mapper;
          
        }
        public void OnGet()
        {
            EditModel.Groupname = "selfservice_tab";
        }
    }
}
