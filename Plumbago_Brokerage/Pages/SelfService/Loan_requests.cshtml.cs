using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plumbago_Brokerage.Data.Global;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.ViewComponents;

namespace Plumbago_Brokerage.Pages.SelfService
{
    public class Loan_requestsModel : PageModel
    {

        [BindProperty(SupportsGet = true)]
        public TableListVM TableList { get; set; }
        public string ErrMsg { get; set; }
        [BindProperty]
        public List<int> SelectedIDs { get; set; }

        public override void OnPageHandlerExecuting(PageHandlerExecutingContext context)
        {
            if (HttpContext.Session.GetInt32("Curruserid") == null)
            {
                Response.Redirect("/Welcome/Login");
            }
        }
        public void OnGet()
        {
            var user = new UsersRepo().GetUserByID(HttpContext.Session.GetInt32("Curruserid"));

            TableList.Groupname = "selfservice_loan";
            //set searchfilter to search by employeeid only          
            TableList.Loans = new LoanRepo().GetAllLoanByEmployeeID(user.EmployeeId);
        }

        public IActionResult OnPostDelete(List<int> SelectedIDs)
        {
            ModMain mod = new ModMain();
            var loanrepo = new LoanRepo();
            foreach (var item in SelectedIDs)
            {
                loanrepo.DeleteLoanByID(item);
            }
            TempData["success"] = true;
            TempData["message"] = mod.GetSuccessMessage();
            return RedirectToPage("Loan_requests");

        }
    }
}
