﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.ViewComponents
{
    public class PageEditModel
    {
        public string ErrMsg { get; set; }
        public string ErrMsgType { get; set; }
        public string PageTitle { get; set; }
        public bool IsDisabled { get; set; }

        public string Groupname { get; set; }

        public string Editid { get; set; }
    }
}
