﻿using Plumbago_Brokerage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.ViewComponents
{
    public class NavbarModel
    {
        public List<Roles> UserRoles { get; set; }

        public long UserID { get; set; }
    }
}
