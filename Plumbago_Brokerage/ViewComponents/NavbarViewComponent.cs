﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Plumbago_Brokerage.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Plumbago_Brokerage.ViewComponents
{
    public class NavbarViewComponent : ViewComponent
    {


        public IViewComponentResult Invoke()
        {
            //var username = HttpContext.Session.GetString("Curruser");
            //return new RedirectToPageResult("");
            NavbarModel model = new NavbarModel();
            return View(model);
        }
    }
}
