﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.ViewComponents

{
    public class SearchFilterModel
    {

        public string RiskID { get; set; }
        public string Option { get; set; }
        public string Status { get; set; }
        public string Department { get; set; }
        public string Branch { get; set; } = "NULL"; //set default value
        public string Period { get; set; }  //set default value
        public string Grade { get; set; } = "NULL"; //set default value
        public IEnumerable<SelectListItem> Branches { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }
        public IEnumerable<SelectListItem> Paysheets { get; set; }
        public IEnumerable<SelectListItem> Periods { get; set; }
        public IEnumerable<SelectListItem> Grades { get; set; }
        public int? Paysheet { get; set; } = 0;

        public string Contains { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public string Groupname { get; set; }

    }
}
