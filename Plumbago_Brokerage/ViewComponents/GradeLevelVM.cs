﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.ViewComponents
{
    public class GradeLevelVM
    {
        public IEnumerable<SelectListItem> PaySheets { get; set; }
        public string SheetId { get; set; }
        public string GradeId { get; set; }


    }
}
