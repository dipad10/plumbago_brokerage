﻿
using Plumbago_Brokerage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.ViewComponents
{
    public class TableListVM
    {

        public IQueryable<Users> Users { get; set; }

        public IQueryable<UserRoles> UserRoles { get; set; }

        public IQueryable<Branches> Branches { get; set; }
        public IQueryable<PayAllowance> payAllowances { get; set; }
        public IQueryable<PayDeductions> payDeductions { get; set; }

        public IQueryable<Employees> Employees { get; set; }
        public IQueryable<Kpi> Kpis { get; set; }
        public IQueryable<PaySlips> Payslips { get; set; }

        public IQueryable<Loans> Loans { get; set; }
        public IQueryable<Leave> Leave { get; set; }
        public IQueryable<Expenses> Reimbursements { get; set; }
        public IQueryable<Appraisal> Appraisals { get; set; }
        public IQueryable<Trainings> TrainingNeeds { get; set; }
        public IQueryable<Roles> Roles { get; set; }
        public IEnumerable<Education> Educations { get; set; }
        public IEnumerable<Hobbies> Hobbies { get; set; }
        public IEnumerable<Promotion> Promotions { get; set; }
        public IEnumerable<Documents> Documents { get; set; }
        public IEnumerable<MedDetails> MedDetails { get; set; }
        public IQueryable<MedDetails> MedDetailsQueryable { get; set; }
        public IQueryable<Medical> MedicalsQueryable { get; set; }
        public IQueryable<Promotion> PromotionsQueryable { get; set; }

        public string Groupname { get; set; }
        public string Groupname2 { get; set; }
        public string Groupname3 { get; set; }
    }
}
