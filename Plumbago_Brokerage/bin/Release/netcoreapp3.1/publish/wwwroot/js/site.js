﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

//goback button

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


$(document).ready(function () {
    $("#hr_promotions_edit").click(function () {
        var id = getParameterByName('id');
        if (id == null) {
            /* alert("Not available until you select an employee.");*/
            /*   alert("Not available until you select an employee.");*/
            toastr.error("Oops!", "Not available until you select an employee");
            event.preventDefault();
        }
    });

    $("#hr_qualifications_edit").click(function () {
        var id = getParameterByName('id');
        if (id == null) {
            toastr.error("Oops!", "Not available until you select an employee");
            event.preventDefault();
        }
    });

    $("#hr_documents_edit").click(function () {
        var id = getParameterByName('id');
        if (id == null) {
            toastr.error("Oops!", "Not available until you select an employee");
            event.preventDefault();
        }
    });
});