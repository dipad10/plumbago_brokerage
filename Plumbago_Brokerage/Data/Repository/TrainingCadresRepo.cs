﻿using Plumbago_Brokerage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.Data.Repository
{
    public class TrainingCadresRepo
    {
        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();




        public List<TrainingCadres> GetAllTrainingCadres()
        {
            return _context.TrainingCadres.OrderBy(p => p.Code).ToList();
        }
    }
}
