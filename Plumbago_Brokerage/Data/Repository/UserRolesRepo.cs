﻿using Plumbago_Brokerage.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace Plumbago_Brokerage.Data.Repository
{
    public class UserRolesRepo
    {
        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();

        public UserRoles AddUserRole(UserRoles userroles)
        {
             _context.UserRoles.Add(userroles);
            _context.SaveChanges();
            return userroles;

        }

        public int CreateUserRole(UserRoles roles, string curruser)
        {
            int code = 0;
            try
            {
                roles.SubmittedOn = DateTime.Now;
                roles.SubmittedBy = curruser;         
                _context.UserRoles.Add(roles);
                code = _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error($"Failed to Save role: {ex.Message}");
            }

            return code;
        }


        public List<Roles> GetUserRoles(long UserID)
        {
            var result = from userrole in _context.UserRoles where userrole.UserId == UserID
                         join roles in _context.Roles on userrole.RoleId equals roles.RoleId
                         select
                         new Roles
                         {
                             RoleName = roles.RoleName,
                            RoleId = userrole.RoleId
                         };


            return result.ToList();
        }

        public bool HasRole(string rolename, long UserID)
        {
            foreach (var item in GetUserRoles(UserID))
            {
                if(rolename == item.RoleName)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
