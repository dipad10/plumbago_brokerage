﻿using Plumbago_Brokerage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.Data.Repository
{
    public class RolesRepo
    {

        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();
        public string HumanResources { get; set; } = "HumanResources";
        public string Payroll { get; set; } = "Payroll";
        public string Admin { get; set; } = "Admin";
        public string Reports { get; set; } = "Reports";

        public string SelfService { get; set; } = "SelfService";
        public string GeneralSettings { get; set; } = "GeneralSettings";
        public List<Roles> GetUserRoles(long UserID)
        {
            var result = from userrole in _context.UserRoles
                         where userrole.UserId == UserID
                         join roles in _context.Roles on userrole.RoleId equals roles.RoleId
                         select
                         new Roles
                         {
                             RoleName = roles.RoleName,
                             RoleId = userrole.RoleId
                         };


            return result.ToList();
        }

        public bool HasRole(string rolename, long UserID)
        {
            foreach (var item in GetUserRoles(UserID))
            {
                if (rolename == item.RoleName)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
