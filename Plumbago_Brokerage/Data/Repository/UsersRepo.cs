﻿using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace Plumbago_Brokerage.Data.Repository
{
    public class UsersRepo
    {
        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();


        public int CreateUser(Users user, string curruser)
        {
            int code = 0;
            try
            {
                    user.SubmittedOn = DateTime.Now;
                    user.SubmittedBy = curruser;
                    var createduser = _context.Users.Add(user);
                     _context.SaveChanges();
                code = Convert.ToInt32(user.UserId);
            }
                catch (Exception ex)
                {
                    Log.Error($"Failed to Save user: {ex.Message}");
                }

            return code;
        }

        public int UpdateUser(Users user, string curruser)
        {
            //Update
            int code = 0;
                try
                {
                    user.ModifiedOn = DateTime.Now;
                    user.ModifiedBy = curruser;
                    _context.Entry(user).State = EntityState.Modified;
                    code = _context.SaveChanges();

                }
                catch (Exception ex)
                {
                    Log.Error($"Failed to Save user: {ex.Message}");
                }
            return code;
        }
           
        
        public Users GetUserByUsername(string Username)
        {
            return _context.Users.Where(p => p.Username == Username).FirstOrDefault();

        }
        public Users GetUserByID(long? id)
        {
            return _context.Users.Where(p => p.UserId == id).FirstOrDefault();

        }

        public IEnumerable<Users> GetUsers()
        {
            return _context.Users.ToList();
        }

        public long GetUserIDByUsername(string username)
        {

            var userid = _context.Users.Where(p => p.Username == username).Select(o => o.UserId).FirstOrDefault();
            return userid;
        }

        public bool IsUserExists(string Username, string Password)
        {
            var user = _context.Users.Where(p => p.Username == Username && p.Password == Password).FirstOrDefault();
            return user != null;
        }

        public IQueryable<Users> GetUsersBySearchFilter(SearchFilterModel searchFilter)
        {
            //if there is filter select all aby filter
            var result = _context.Users.AsQueryable().OrderByDescending(p => p.SubmittedOn).Take(50);

            if (searchFilter != null)
            {

                if (!string.IsNullOrEmpty(searchFilter.Option) && searchFilter.Option == "Fullname")
                    result = result.Where(x => x.FirstName.Contains(searchFilter.Contains) || x.LastName.Contains(searchFilter.Contains));
                if (!string.IsNullOrEmpty(searchFilter.Option) && searchFilter.Option == "Username")
                    result = result.Where(x => x.Username.Contains(searchFilter.Contains));
                if (searchFilter.From.HasValue)
                    result = result.Where(x => x.SubmittedOn >= searchFilter.From);
                if (searchFilter.To.HasValue)
                    result = result.Where(x => x.SubmittedOn <= searchFilter.To);               

            }

            return result.OrderByDescending(p => p.SubmittedOn);

         
        }

    }
}
