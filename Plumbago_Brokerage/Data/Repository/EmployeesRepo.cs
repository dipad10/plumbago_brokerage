﻿using Microsoft.EntityFrameworkCore;
using Plumbago_Brokerage.Data;
using Plumbago_Brokerage.Models;
using Plumbago_Brokerage.ViewComponents;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlumbagoHR.Data.Repository
{
    public class EmployeesRepo
    {

        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();



        public Employees GetEmployeeByID(string id)
        {
            return _context.Employees.Where(p => p.EmployeeId == id).FirstOrDefault();
        }

        public int CreateEmployee(Employees employee, string curruser)
        {
            int code = 0;
            try
            {
                employee.SubmitOn = DateTime.Now;
                employee.Submitby = curruser;
                employee.Deleted = 0;
                employee.Active = 1;
                _context.Employees.Add(employee);
                code = _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error($"Failed to Save Employee: {ex.Message}");
            }

            return code;
        }

        public int UpdateEmployee(Employees employee, string curruser)
        {
            //Update
            int code = 0;
            try
            {
                employee.SubmitOn = DateTime.Now;
                employee.Submitby = curruser;
                employee.Deleted = 0;
                employee.Active = 1;
                _context.Entry(employee).State = EntityState.Modified;
                code = _context.SaveChanges();
                 
            }
            catch (Exception ex)
            {
                Log.Error($"Failed to Update kpi: {ex.Message}");
            }
            return code;
        }

        public IEnumerable<Employees> GetAllEmployeesByID(string id)
        {
            return _context.Employees.Where(p => p.EmployeeId == id).ToList();
        }




        public IEnumerable<Employees> GetAllEmployees()
        {
            return _context.Employees.OrderByDescending(p => p.EmployeeId).ToList();
        }

        public IQueryable<Employees> GetEmployeesBySearchFilter(SearchFilterModel searchFilter)
        {
            //if there is filter select all aby filter
            var result = _context.Employees.AsQueryable().OrderByDescending(p => p.SubmitOn).Take(50);
            if (searchFilter != null)
            {

                if (!string.IsNullOrEmpty(searchFilter.Option) && searchFilter.Option == "Name")
                    result = result.Where(x => x.Surname.Contains(searchFilter.Contains) || x.OtherNames.Contains(searchFilter.Contains));
                //if (!string.Equals(searchFilter.Paysheet, "NULL"))
                //    result = result.Where(x => x.Paysheet == searchFilter.Paysheet);
                if (searchFilter.Paysheet != 0)
                    result = result.Where(x => x.SheetId2 == searchFilter.Paysheet);
                if (!string.IsNullOrEmpty(searchFilter.Option) && searchFilter.Option == "EmployeeID")
                    result = result.Where(x => x.EmployeeId == searchFilter.Contains);
                if (!string.Equals(searchFilter.Branch, "NULL"))
                    result = result.Where(x => x.Branch == searchFilter.Branch);
                if (searchFilter.From.HasValue)
                    result = result.Where(x => x.SubmitOn >= searchFilter.From);
                if (searchFilter.To.HasValue)
                    result = result.Where(x => x.SubmitOn <= searchFilter.To);

            }

            return result.OrderByDescending(p => p.SubmitOn);

        }
    }
}
