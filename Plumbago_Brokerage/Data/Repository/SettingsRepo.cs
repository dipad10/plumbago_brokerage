﻿using Plumbago_Brokerage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.Data.Repository
{
    public class SettingsRepo
    {
        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();


        public Settings GetCompanySettings()
        {
            return _context.Settings.FirstOrDefault();
        }

    }
}
