﻿using Plumbago_Brokerage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.Data.Repository
{
    public class TrainingNeedsRepo
    {
        private Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();

        public IQueryable<Trainings> GetTrainingNeedsByEmployeeID(string EmployeeID)
        {
            return _context.Trainings.Where(p => p.EmployeeId == EmployeeID).OrderByDescending(p => p.TrainHisId);
        }
    }
}
