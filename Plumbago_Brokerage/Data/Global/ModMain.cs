﻿using Plumbago_Brokerage.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.VisualBasic;
using Plumbago_Brokerage.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using PlumbagoHR.Data.Repository;
using Plumbago_Brokerage.Reports.Payroll;
using Serilog;

namespace Plumbago_Brokerage.Data.Global
{
    public class ModMain
    {

        Plumbago_Brokerage_DBContext _context = new Plumbago_Brokerage_DBContext();
       
        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

       public enum ROLESIDS
       {
            SelfService = 5494,
            GeneralSettings = 5495,
            Reports = 5493,
            Admin = 5492,
            Payroll = 5491,
            HumanResources = 5489
        }

        #region MAILSECTION
        public string PopulateBodyEmployeePayslip(string fullname, string Payslipmonth)
        {
            var path = Path.GetFullPath("~/wwwroot/Templates/EmailTemplateEmployeePayslip.html").Replace("~\\", "");
            //var result = System.IO.Path.Combine(path, @"\Templates\EmailTemplateEmployeePayslip.html");
            StreamReader reader = new StreamReader(path);
            string body = reader.ReadToEnd();
            body = body.Replace("{Fullname}", fullname);
            body = body.Replace("{Payslipmonth}", Payslipmonth);           
            return body;
        }

        public string PopulateBodyLoanRaised(string fullname, string Loanname, string Amount, string InterestRate, string InterestAmount, string TotalAmountDue, string Startdate, string Enddate, string Status)
        {
            var path = Path.GetFullPath("~/wwwroot/Templates/EmailTemplateLoanRequestRaised.html").Replace("~\\", "");
            //var result = System.IO.Path.Combine(path, @"\Templates\EmailTemplateEmployeePayslip.html");
            StreamReader reader = new StreamReader(path);
            string body = reader.ReadToEnd();
            body = body.Replace("{Fullname}", fullname);
            body = body.Replace("{Loanname}", Loanname);
            body = body.Replace("{Amount}", Amount);
            body = body.Replace("{InterestRate}", InterestRate);
            body = body.Replace("{InterestAmount}", InterestAmount);
            body = body.Replace("{TotalAmountDue}", TotalAmountDue);
            body = body.Replace("{Startdate}", Startdate);
            body = body.Replace("{Enddate}", Enddate);
            body = body.Replace("{Status}", Status);
            return body;
        }
        public string PopulateBodyLoanRaisedHR(string fullname, string Employeename, string Loanname, string Amount, string InterestRate, string InterestAmount, string TotalAmountDue, string Startdate, string Enddate, string Status)
        {
            var path = Path.GetFullPath("~/wwwroot/Templates/EmailTemplateLoanRequestRaisedHR.html").Replace("~\\", "");
            //var result = System.IO.Path.Combine(path, @"\Templates\EmailTemplateEmployeePayslip.html");
            StreamReader reader = new StreamReader(path);
            string body = reader.ReadToEnd();
            body = body.Replace("{Fullname}", fullname);
            body = body.Replace("{Employeename}", Employeename);
            body = body.Replace("{Loanname}", Loanname);
            body = body.Replace("{Amount}", Amount);
            body = body.Replace("{InterestRate}", InterestRate);
            body = body.Replace("{InterestAmount}", InterestAmount);
            body = body.Replace("{TotalAmountDue}", TotalAmountDue);
            body = body.Replace("{Startdate}", Startdate);
            body = body.Replace("{Enddate}", Enddate);
            body = body.Replace("{Status}", Status);
            return body;
        }
        public string PopulateBodyEmployeeLogin(string fullname, string Username, string Password)
        {
            var path = Path.GetFullPath("~/wwwroot/Templates/EmailTemplateEmployeeLogin.html").Replace("~\\", "");
            //var result = System.IO.Path.Combine(path, @"\Templates\EmailTemplateEmployeePayslip.html");
            StreamReader reader = new StreamReader(path);
            string body = reader.ReadToEnd();
            body = body.Replace("{Fullname}", fullname);
            body = body.Replace("{Username}", Username);
            body = body.Replace("{Password}", Password);
            return body;
        }

        public string PopulateBodyLoanApproved(string fullname, string Loanname, string Amount, string TotalAmountDue, string Startdate, string Enddate, string MonthlyLoanAmt, string Status)
        {
            var path = Path.GetFullPath("~/wwwroot/Templates/EmailTemplateLoanApproved.html").Replace("~\\", "");
            //var result = System.IO.Path.Combine(path, @"\Templates\EmailTemplateEmployeePayslip.html");
            StreamReader reader = new StreamReader(path);
            string body = reader.ReadToEnd();
            body = body.Replace("{Fullname}", fullname);
            body = body.Replace("{Loanname}", Loanname);
            body = body.Replace("{Amount}", Amount);           
            body = body.Replace("{TotalAmountDue}", TotalAmountDue);
            body = body.Replace("{MonthlyLoanAmt}", MonthlyLoanAmt);
            body = body.Replace("{Startdate}", Startdate);
            body = body.Replace("{Enddate}", Enddate);
            body = body.Replace("{Status}", Status);
            return body;
        }

        public bool SendHtmlFormattedEmail(string recepientEmail, string cc, string subject, string body, Settings companysettings, Attachment attachment)
        {
            try
            {
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                mailMessage.From = new MailAddress(companysettings.SmtpEmail, companysettings.SmtpSenderName);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                if (cc != "")
                {
                    string[] CCId = cc.Split(',');

                    foreach (string CCEmail in CCId)
                        // Adding Multiple CC email Id
                        mailMessage.CC.Add(new MailAddress(CCEmail));
                }
                else
                {
                }
                if (attachment != null)
                {
                    mailMessage.Attachments.Add(attachment);
                }
                SmtpClient smtp = new SmtpClient();
                smtp.Host = companysettings.SmtpHost;
                smtp.EnableSsl = (bool)companysettings.SmtpEnableSsl;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = companysettings.SmtpEmail;
                NetworkCred.Password = companysettings.SmtpPaswd;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = (int)companysettings.SmtpPort;
                smtp.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error($"Error occured while sending mail {ex.Message}");
                return false;
            }
        }

        #endregion

        #region GETFUNCTIONS
        public string GetNextValue(string Numtype)
        {
            var rec = _context.AutoNumbers.Where(o => o.NumType.Equals(Numtype)).SingleOrDefault();
           var nextvalue = rec.NextValue.ToString();
            UpdateAuto(Numtype);
            return nextvalue;
        }

        public string GetEmployeeNo(string Numtype)
        {
            var nextvalue = GetNextValue(Numtype);
            var employeeno = GetRandomNumber().ToString() + nextvalue;         
            return employeeno;
        }

        public void UpdateAuto(string Numtype)
        {
            var rec = _context.AutoNumbers.Where(o => o.NumType.Equals(Numtype)).SingleOrDefault();
            rec.NextValue = rec.NextValue + 1;
            _context.SaveChanges();
        }

        public string GenerateReceiptNumber(string Numtype)
        {
            var nextvalue = GetNextValue(Numtype);
            string format = $"RCP/{DateTime.Now.Year.ToString("yy")}/{DateTime.Now.Month.ToString("MM")}/{nextvalue}";
            return format;
        }

        public string GenerateBoookingNumber(string Numtype)
        {
            var nextvalue = GetNextValue(Numtype);
            string format = $"BK/{DateTime.Now.Year.ToString("yy")}/{DateTime.Now.Month.ToString("MM")}/{GetRandomNumber()}/{nextvalue}";
            return format;
        }
        public string GetBranchByID(string id)
        {

            return _context.Branches.Where(p => p.BranchId == Convert.ToInt32(id)).Select(a => a.Description).FirstOrDefault();
        }

        public bool IsPayrollAlreadyDoneForEmployee(string Employeeid, string period)
        {

            var rec = _context.PaySlips.Where(p => p.EmployeeId == Employeeid && p.Period == period).FirstOrDefault();
            if (rec != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetUnitByID(string id)
        {

            return _context.Units.Where(p => p.Code == id).Select(a => a.Section).FirstOrDefault();
        }
        public string GetGradeByID(string id)
        {
            return _context.PayGrade.Where(p => p.Id == Convert.ToInt32(id)).Select(a => a.Name).FirstOrDefault();
        }

        public string GetEmployeeNameByID(string id)
        {
            var emp = _context.Employees.Where(p => p.EmployeeId == id).FirstOrDefault();
            return $"{emp.OtherNames} {emp.Surname}";
        }
        public string GetPaySheetByID(string id)
        {
            return _context.PaySheets.Where(p => p.SheetId == Convert.ToInt32(id)).Select(a => a.PaySheet).FirstOrDefault();
        }
        public string GetAllowanceNameByID(string id)
        {
            return _context.AllowanceId.Where(p => p.AllowId == Convert.ToInt32(id)).Select(a => a.AllowName).FirstOrDefault();
        }

        public string GetDeductionNameByID(string id)
        {
            return _context.DeductionId.Where(p => p.DedId == Convert.ToInt32(id)).Select(a => a.DedName).FirstOrDefault();
        }
        public string GetDepartmentByID(string id)
        {
            var name = _context.Departments.Where(p => p.Code == id).Select(a => a.Name).FirstOrDefault();
            return name;
        }


        public string GetErrorMessage()
        {
            return "An Error Occured while saving Data, Please contact your Administrator.";
        }

        public string GetInvalidLoginMessage()
        {
            return "Invalid username or password";
        }

        public string GetErrMessageTypeError()
        {
            return "error";
        }

        public string GetErrMessageTypeInfo()
        {
            return "info";
        }
        public string GetSuccessMessage()
        {
            return "Data Saved Succesfully!";
        }

        public string GetDeletedMessage()
        {
            return "Data Deleted Succesfully!";
        }
        public string GetStatusApproved()
        {
            return "APPROVED";
        }

        public string GetStatusPending()
        {
            return "PENDING";
        }

        public int GetDaysBetween2Dates(DateTime startdate, DateTime enddate)
        {
            return (enddate - startdate).Days;
        }

        public int GetRandomNumber()
        {
            Random rand = new Random();
           return rand.Next(1001, 9999);
        }

        public string GetRandomString()
        {
            Random rand = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 5)
              .Select(s => s[rand.Next(s.Length)]).ToArray());
        }

        public IEnumerable<Branches> GetBranches()
        {
            return _context.Branches.OrderByDescending(p => p.BranchId).ToList();
        }

        public decimal? GetInterestRatebyduration(string duration)
        {
            return _context.LoanDurations.Where(p => p.Duration == duration).Select(p => p.InterestRate).FirstOrDefault();
        }

    

        public bool ProcessEmployeePayslipToEmail(IEnumerable<PaySlips> payslips)
        {
            var setting = new SettingsRepo().GetCompanySettings();
            var employeerepo = new EmployeesRepo();
            bool mailsuccessfull = false;
            try
            {
                foreach (var rec in payslips)
                {
                    //First of all get the details of this employee
                    var empdetails = employeerepo.GetEmployeeByID(rec.EmployeeId);

                    RptEmployeePaySlip report = new RptEmployeePaySlip();
                    //Obtain a parameter, and set its value.
                    report.Parameters["Emp_Id"].Value = rec.EmployeeId;
                    report.Parameters["Month"].Value = rec.Period;

                    // Create a new memory stream and export the report in PDF.
                    MemoryStream stream = new MemoryStream();
                    report.ExportToPdf(stream);
                    // Create a new attachment and add the PDF document.
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                    Attachment attachedDoc = new Attachment(stream, "Payslip.pdf", "application/pdf");
                    //setup mail
                    var fullname = $"{empdetails.OtherNames} {empdetails.Surname}";
                    string body = PopulateBodyEmployeePayslip(fullname, rec.Period);
                    mailsuccessfull = SendHtmlFormattedEmail(empdetails.Email, "", "PaySlip", body, setting, attachedDoc);

                }
            }
            catch (Exception ex)
            {

                Log.Error($"An error occured while processing payslip to employee mail: {ex}");
            }
          
            if (mailsuccessfull == true)
            {
                return true;
            }
            return false;
        }

        public decimal? GetDefaultInterestRate()
        {
            var rate = _context.LoanDurations.OrderBy(p => p.Duration).Select(p => p.InterestRate).Take(1).FirstOrDefault();
            if (rate == null)
            {
                return 0;
            }
            return rate;
        }

        public string GetLoanNextMonth(DateTime? startdate)
        {
            var nextmonth = startdate.Value.AddMonths(1).ToString("MMMM yyyy");
            return nextmonth;
        }
        public PaySheets GetPaySheetByGrade(string Grade)
        {
            var paygrade = _context.PayGrade.Where(p => p.Name == Grade).FirstOrDefault();
            var paysheet = _context.PaySheets.Where(a => a.SheetId == paygrade.SheetId).FirstOrDefault();
            return paysheet;
        }

        public List<Employees> GetAllHREmployeesUnit()
        {
            var rec = _context.Employees.Where(p => p.Unit == "HUMAN RESOURCES").ToList();
            
            return rec;
        }

        public Employees GetEmployeeProfileById(string EmployeeID)
        {
            return _context.Employees.Where(p => p.EmployeeId == EmployeeID).FirstOrDefault();
        }
        #endregion

        #region FORMATTER
        public string FormatNaira(object Num)
        {
            if (Num == null)
            {
                return "0";
            }
            return string.Format("₦{0:n}", Num);

        }

        public string FormatNumber(object Num)
        {
            if (Num == null)
            {
                return "0";
            }
            return string.Format("{0:n}", Num);

        }


        public string FormatDate(DateTime date)
        {
            return date.ToString("dd MMM yyyy");
        }
       
        #endregion

        #region POPULATEDROPDOWNS


        public SelectList PopulateLeaveTypes()
        {
            var leaves = new LeaveTypeRepo().GetLeaveTypes();
            return new SelectList(leaves, "LeaveType1", "LeaveType1");
        }

        public SelectList PopulateLoanTypes()
        {
            var loantypes = new LoanTypeRepo().GetLoanTypes();
            
            return new SelectList(loantypes, "LoanType1", "LoanType1");
        }

        public SelectList PopulateLoanDuration()
        {
            var loanDurations = new LoanDurationRepo().GetAllLoanDuration();
            return new SelectList(loanDurations, "Duration", "Duration");
        }

        public SelectList PopulateBranches()
        {
            var branches = new BranchesRepo().GetBranches();
            return new SelectList(branches, "BranchId", "Description");
        }



        public SelectList PopulateExpenseTypes()
        {
            var Expensetypes = new ExpenseTypesRepo().GetAllExpenseTypes();
            return new SelectList(Expensetypes, "ExpenseTypes1", "ExpenseTypes1");
        }
        public SelectList PopulateTrainingCadres()
        {
            var TrainingCadres = new TrainingCadresRepo().GetAllTrainingCadres();
            return new SelectList(TrainingCadres, "Name", "Name");
        }
        public SelectList PopulatePayGrade()
        {
            var paygrade = new PayGradeRepo().GetPayGrade();
            return new SelectList(paygrade, "Name", "Name");
        } 
        public SelectList PopulatePaySheets()
        {
            var paysheet = new PaysheetsRepo().GetPaySheets();
            return new SelectList(paysheet, "SheetId", "PaySheet");
        }

        public SelectList PopulateStates()
        {
            var states = _context.States.OrderBy(p => p.StateId).ToList();
            return new SelectList(states, "State", "State");
        }

        public SelectList PopulateGradeLevel()
        {
            var grades = _context.PayGrade.OrderBy(p => p.Id).ToList();
            return new SelectList(grades, "Id", "Name");
        }
        public SelectList PopulateSteps()
        {
            var steps = _context.Steps.OrderBy(p => p.Id).ToList();
            return new SelectList(steps, "Name", "Name");
        }

        public SelectList PopulateDepartments()
        {
            var dept = _context.Departments.OrderBy(p => p.Code).ToList();
            return new SelectList(dept, "Code", "Name");
        }

        public SelectList PopulateUnits()
        {
            var unit = _context.Units.OrderBy(p => p.Code).ToList();
            return new SelectList(unit, "Code", "Section");
        }

        public SelectList PopulateAllowances()
        {
            var allowances = _context.PayAllowance.OrderBy(p => p.Hid).ToList();
            return new SelectList(allowances, "AllowName", "AllowName");
        }
        public SelectList PopulateAllowanceIDs()
        {
            var allowances = _context.AllowanceId.OrderBy(p => p.AllowId).ToList();
            return new SelectList(allowances, "AllowId", "AllowName");
        }

        public SelectList PopulateDeductionIDs()
        {
            var deductions = _context.DeductionId.OrderBy(p => p.DedId).ToList();
            return new SelectList(deductions, "DedId", "DedName");
        }
        public SelectList PopulateAppraisalGrades()
        {
            var grades = _context.AppraisalGrades.OrderBy(p => p.Id).ToList();
            return new SelectList(grades, "AppraisalName", "AppraisalName");
        }

        public SelectList PopulateDeductions()
        {
            var deductions = _context.PayDeductions.OrderBy(p => p.Hid).ToList();
            return new SelectList(deductions, "DedName", "DedName");
        }


        public List<SelectListItem> PopulatePFA()
        {
            return new List<SelectListItem>{
            new SelectListItem{ Text="RIB Pension Fund Managers Limited", Value = "RIB Pension Fund Managers Limited"},
            new SelectListItem{ Text="AXA Mansard Pension Limited", Value = "AXA Mansard Pension Limited"},
            new SelectListItem{ Text="Sigma Pensions Limited", Value = "Sigma Pensions Limited"}
        };
        }

        public List<SelectListItem> PopulateBanks()
        {
            return new List<SelectListItem>{
            new SelectListItem{ Text="DIAMOND BANK PLC", Value = "DIAMOND BANK PLC"},
            new SelectListItem{ Text="FIRST BANK OF NIGERIA PLC", Value = "FIRST BANK OF NIGERIA PLC"},
            new SelectListItem{ Text="SKYE BANK PLC", Value = "SKYE BANK PLC"},
            new SelectListItem{ Text="STERLING BANK PLC", Value = "STERLING BANK PLC"},
            new SelectListItem{ Text="UNITY BANK PLC", Value = "UNITY BANK PLC"},
            new SelectListItem{ Text="ZENITH BANK PLC", Value = "ZENITH BANK PLC"},
            new SelectListItem{ Text="GTBANK  PLC", Value = "GTBANK  PLC"},
            new SelectListItem{ Text="FCMB PLC", Value = "FCMB PLC"}
        };
        }

        public List<SelectListItem> PopulateHMO()
        {
            return new List<SelectListItem>{
            new SelectListItem{ Text="Royal Exchange", Value = "Royal Exchange"},
            new SelectListItem{ Text="Hygia HMO", Value = "Hygia HMO"},
            new SelectListItem{ Text="LifeCare Partners", Value = "LifeCare Partners"}
        };

        }

        public List<SelectListItem> PopulateCertifications()
        {
            return new List<SelectListItem>{
            new SelectListItem{ Text="WAEC", Value = "WAEC"},
            new SelectListItem{ Text="NECO", Value = "NECO"},
            new SelectListItem{ Text="JAMB", Value = "JAMB"},
            new SelectListItem{ Text="Bachelors", Value = "Bachelors"},
            new SelectListItem{ Text="Masters", Value = "Masters"},
            new SelectListItem{ Text="Phd", Value = "Phd"}
        };

        }

        public SelectList PopulateAppraisalscores()
        {
            var scores = _context.AppraisalScore.OrderBy(p => p.Id).ToList();
            return new SelectList(scores, "Id", "Description");
        }

       

        public SelectList PopulatEmployees()
        {
            var employees = _context.Employees.OrderBy(p => p.Grade).Select(s => new
            {
                Employeeid = s.EmployeeId,
                Fullname = $"{s.Surname} {s.OtherNames}, {s.Grade}, {s.Branch}"
            }).ToList();
            return new SelectList(employees, "Employeeid", "Fullname");
        }

        public SelectList PopulatePrimaryManagers()
        {
            var employees = _context.Employees.OrderBy(p => p.Grade).Select(s => new
            {
                Employeeid = s.EmployeeId,
                Displayname = $"{s.Surname} {s.OtherNames}, {s.Grade}, {s.Branch}",
                Fullname = $"{s.Surname} {s.OtherNames}"
            }).ToList();
            return new SelectList(employees, "Fullname", "Displayname");
        }


        public SelectList PopulatePeriod()
        {
            List<string>  dates = new List<string>();
            for (int i = -6; i < 6; i++)
            {
               dates.Add(DateTime.Now.AddMonths(i).ToString("MMMM yyyy"));
            }
            return new SelectList(dates);
        }

        public SelectList PopulatePeriodOld()
        {
            List<string> dates = new List<string>();
            for (int i = -12; i < 6; i++)
            {
                dates.Add(DateTime.Now.AddMonths(i).ToString("MMMM yyyy"));
            }
            return new SelectList(dates);
        }
        public SelectList PopulatePreviousMonth()
        {
           List<string> dates = new List<string>();
           dates.Add(DateTime.Now.AddMonths(-1).ToString("MMMM yyyy"));
            
            return new SelectList(dates);
        }

        public SelectList PopulateNextMonths()
        {
            List<string> dates = new List<string>();
            for (int i = 0; i < 6; i++)
            {
                dates.Add(DateTime.Now.AddMonths(i).ToString("MMMM yyyy"));
            }
            return new SelectList(dates);
        }

        #endregion

    }
}
