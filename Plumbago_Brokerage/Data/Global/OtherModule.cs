﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plumbago_Brokerage.Data.Global
{
    public class OtherModule
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        public OtherModule(IWebHostEnvironment env)
        {
            _hostingEnvironment = env;
        }

        public string GetProjectRootpath()
        {
            return _hostingEnvironment.ContentRootPath;
        }
    }
}
