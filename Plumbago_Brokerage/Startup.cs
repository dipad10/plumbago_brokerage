using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Reporting;
using Plumbago_Brokerage.Data.Repository;
using Plumbago_Brokerage.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Plumbago_Brokerage.Data.Global;
using PlumbagoHR.Data.Repository;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace Plumbago_Brokerage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Plumbago_Brokerage_DBContext>
         (options => options.UseSqlServer(Configuration["ConnectionStrings:API"]));
            services.AddRazorPages().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddMvc().AddRazorRuntimeCompilation();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(20);
                options.Cookie.HttpOnly = true;
            });
            services.AddMvc().AddRazorPagesOptions(options =>
            {
                options.Conventions.AddPageRoute("/Welcome/Login", "");
               
            });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddTransient<UsersRepo>();
            services.AddTransient<LeaveRepo>();
            services.AddTransient<LeaveTypeRepo>();
            services.AddTransient<LoanRepo>();
            services.AddTransient<PayGradeRepo>();
            services.AddTransient<LeaveTypeRepo>();
            services.AddTransient<PaysheetsRepo>();
            services.AddTransient<EmployeesRepo>();
            services.AddTransient<RolesRepo>();
            services.AddTransient<UserRolesRepo>();        
            services.AddTransient<BranchesRepo>();
            services.AddTransient<ModMain>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();


            //Devexpress section
            services.AddDevExpressControls();
            services.AddMvc()
            .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);
            services.ConfigureReportingServices(configurator => {
                configurator.ConfigureWebDocumentViewer(viewerConfigurator => {
                    viewerConfigurator.UseCachedReportSourceBuilder();
                });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            app.UseDevExpressControls();
            if (env.IsDevelopment())
            {
                logger.LogInformation("In Development..");
                app.UseDeveloperExceptionPage();
            }
            else
            {
                logger.LogInformation("In Production..");
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "node_modules")),
                RequestPath = "/node_modules"
            });
            app.UseSession();
            //redirect user if session is empty
            app.Use(async (context, next) =>
            {
                string CurrentUsernameSession = context.Session.GetString("Curruser");
                if (!context.Request.Path.Value.Contains("/Welcome/Login"))
                {
                    if (string.IsNullOrEmpty(CurrentUsernameSession))
                    {
                        var path = $"/Welcome/Login?ReturnUrl={context.Request.Path}";
                        context.Response.Redirect(path);
                        return;
                    }

                }
                await next();
            });

            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();

                endpoints.MapControllerRoute( //<-- FOR DEVEXPRESS REPORTING
                  name: "default",
                  pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
