﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace Plumbago_Brokerage.Models
{
    public partial class TreatyProportions
    {
        public long PropId { get; set; }
        public string SubriskId { get; set; }
        public string PartyId { get; set; }
        public long UnderYr { get; set; }
        public decimal? PremRate { get; set; }
        public decimal? ComRate { get; set; }
        public decimal? ProfitComm { get; set; }
        public decimal? MgtExpenseRate { get; set; }
        public decimal? LosslimitRate { get; set; }
        public decimal? PortfolioPrem { get; set; }
        public decimal? PortfolioLoss { get; set; }
        public decimal? A1 { get; set; }
        public decimal? A2 { get; set; }
        public string Tag { get; set; }
        public string SubmitBy { get; set; }
        public DateTime? SubmitOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}