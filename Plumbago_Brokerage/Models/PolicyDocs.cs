﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace Plumbago_Brokerage.Models
{
    public partial class PolicyDocs
    {
        public long Id { get; set; }
        public string PolicyNo { get; set; }
        public string EndorsementNo { get; set; }
        public DateTime? EntryDate { get; set; }
        public string Documents { get; set; }
        public byte? Deleted { get; set; }
        public byte? Active { get; set; }
        public string OptionType { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}