﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace Plumbago_Brokerage.Models
{
    public partial class Budgets
    {
        public long BudgetId { get; set; }
        public string RecType { get; set; }
        public DateTime? BudYear1 { get; set; }
        public DateTime? BudYear2 { get; set; }
        public string RiskCode { get; set; }
        public string RiskName { get; set; }
        public decimal? BudValue { get; set; }
        public string Description { get; set; }
        public string Tag { get; set; }
        public byte? Deleted { get; set; }
        public byte? Active { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}